package nl.scholtens.music.dataInteraction.enums;

import nl.scholtens.music.enums.Style;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class StyleTest {

    @Test
    @DisplayName("Style is 'Pop'")
    public void getStyleTest() {
        String style = Style.S1.getStyle();
        assertThat(style, equalTo("Pop"));
    }

    @Test
    @DisplayName("Waarde is 'S1'")
    public void getValue() {
        Style style = Style.valueOf("S1");
        assertThat(style, equalTo(Style.S1));
    }
}
