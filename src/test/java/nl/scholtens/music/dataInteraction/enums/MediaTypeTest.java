package nl.scholtens.music.dataInteraction.enums;

import nl.scholtens.music.enums.MediaType;
import org.junit.jupiter.api.Test;

import static nl.scholtens.music.enums.MediaType.MT1;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

public class MediaTypeTest {


    @Test
    public void getMediaTypeValueTest() {
        String mediaType = MT1.getMediaType();
        assertThat(mediaType, equalTo("Elpee"));
    }

    @Test
    public void getValue() {
        MediaType mediaType = MediaType.valueOf("MT1");
        assertThat(mediaType, equalTo(MT1));
    }
}
