<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>MyMusic</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">
    <spring:url value="/beheer/albums" var="beheerURL"/>
    <spring:url value="/music" var="cancelURL" />
    <form:form modelAttribute="form" method="post" action="${beheerURL}" enctype="multipart/form-data">
<form>
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
                <jsp:include page="main_menu.jsp"/>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset class="marginleft20 marginright20" ${form.fieldset}>
                <c:if test="${form.importBeheer}">
                    <form:hidden path="importBeheer"/>
                    <legend>
                        <h2 class="h2">Import</h2>
                    </legend>
                    <table class="marginleft20">
                        <tr>
                            <td>
                                <span>Selecteert de bestand(en)</span>
                                </br></br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Selecteer Album - XML file(s)</span>
                            </td>
                            <td>
                                <form:input type="file" name="xmlfiles" accept=".xml" multiple="multiple" path="xmlMultipartFiles" cssClass="btn tdbutton height40 marginleft75" id="xmlfiles"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Selecteer afbeelding(en) </span>
                            </td>
                            <td>
                                <form:input type="file" name="imgfiles" accept=".png, .jpeg, .jpg" multiple="multiple" path="imgMultipartFiles" cssClass="btn tdbutton height40 marginleft75" id="imgfiles"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <button type="submit" class="btn tdbutton marginleft20 margintop25">Importeer</button>
                               <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
                            </td>
                        </tr>
                    </table>
                </c:if>
                <c:if test="${form.exportBeheer}">
                    <form:hidden path="exportBeheer"/>
                    <legend>
                        <h2 class="h2">Export</h2>
                    </legend>
                    <table class="marginleft20">
                        <tr>
                            <td>
                                <span>Alle albums van een artist </span>
                            </td>
                            <td>
                                <form:select path="artist" cssClass="btn tdbutton marginleft75" onchange="this.form.submit()">
                                    <form:option selected="selected" value="NONE" label="--- Kies ---"/>
                                    <c:forEach items="${form.artists}" var="artist">
                                        <form:option value="${artist}">
                                            <c:out value="${artist}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <span>Alle albums van een groep </span>
                            </td>
                            <td>
                                <form:select path="group" cssClass="btn tdbutton marginleft75" onchange="this.form.submit()">
                                    <form:option selected="selected" value="NONE" label="--- Kies ---"/>
                                    <c:forEach items="${form.groups}" var="group">
                                        <form:option value="${group}">
                                            <c:out value="${group}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Een bepaalde album </span>
                            </td>
                            <td>
                                <form:select path="album" cssClass="btn tdbutton marginleft75" >
                                    <form:option selected="selected" value="NONE" label="--- Kies ---"/>
                                    <c:forEach items="${form.albums}" var="album">
                                        <form:option value="${album}">
                                            <c:out value="${album}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                            <td>
                                 <form:select path="media" cssClass="btn tdbutton" id="selectmedia" onchange="this.form.submit()">
                                    <form:option selected="selected" value="NONE" label="--- Kies ---"/>
                                    <c:forEach items="${form.mediaTypes}" var="media">
                                        <form:option value="${media}">
                                            <c:out value="${media}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span>Exporteer alle albums</span>

                            </td>
                            <td>
                                <form:checkbox path="all" cssClass="height40 marginleft75" id="all" name="all" />
                            </td>
                            <td>
                                <form:select path="allMedia" cssClass="btn tdbutton" id="selectmedia" onchange="this.form.submit()">
                                    <form:option selected="selected" value="NONE" label="--- Kies ---"/>
                                    <form:option value="ALL" label="Alles"/>
                                    <c:forEach items="${form.mediaTypes}" var="media">
                                        <form:option value="${media}">
                                            <c:out value="${media}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
                            </td>
                        </tr>
                    </table>
                </c:if>
             </fieldset>
            </td>
        </tr>
        <tr>
            <td>
        <c:if test="${form.result}">
                <fieldset class="marginleft20 marginright20">
                    <legend>
                         <h2 class="h2">Resultaat</h2>
                    </legend>
                  <table class="marginleft20">
                        <tr>
                            <td>
                                 <c:if test="${form.importBeheer}">
                                     <span> ${form.total} album(s) geimporteerd</span>
                                </c:if>
                                <c:if test="${form.exportBeheer}">
                                    <span> ${form.total} album(s) geexporteerd</span>
                                </c:if>
                           </td>
                        </tr>
                        <tr>
                            <td>
                                <c:if test="${form.exportBeheer}">
                                    <span>naar de map ${form.filePath}</span>
                                </c:if>
                           </td>
                        </tr>
                        <tr>
                           <td>
                             <input type="submit" formaction="${cancelURL}" value="Hoofdmenu" class="btn tdbutton"/>
                           </td>
                        </tr>
                  </table>

                </fieldset>
        </c:if>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="footer.jsp"/>
    </form:form>
</body>
</html>