<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Setup</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<spring:url value="/setup/save" var="saveURL"/>
<form:form modelAttribute="setupForm" method="get" action="${saveURL}">
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>
                        <h2 class="h2">Instellingen</h2>
                    </legend>
                    <table class="maintable marginleft20">
                        <tr>
                            <td>
                                <label>Pad voor de Afbeeldingen</label>
                            </td>
                            <td>
                                <form:input type="text" path="path" size="50" cssClass="height40" id="imgpath"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Pad voor import en export</label>
                            </td>
                            <td>
                                <form:input type="text" path="path" size="50" cssClass="height40" id="xmlpath"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <button type="submit" class="btn tdbutton marginleft20 margintop25">Opslaan</button>
    <spring:url value="/music" var="cancelURL"/>
    <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
</form:form>
<jsp:include page="footer.jsp"/>

</body>
</html>