<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Title</title>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<table class="maintable">
    <tr>
        <td class="staritem">
            <span class="smallfont">&copy; 2019,  A. Scholtens</span>
        </td>
        <td class="enditem">
            <span class="smallfont"> versie&nbsp;${sessionScope.get('musicSession').buildVersion}</span>
        </td>
    </tr>
</table>

</body>
</html>