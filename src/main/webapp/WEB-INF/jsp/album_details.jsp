<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>AlbumDetails</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<form>
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
                <jsp:include page="main_menu.jsp"/>
            </td>
        </tr>
        <tr>
            <td>
                <fieldset>
                    <legend>
                        <h2 class="h2">Album Details</h2>
                    </legend>
                    <table class="headtable marginleft20">
                        <tr>
                            <td class="with400">
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="h2">${form.album.title}</td>
                                    </tr>
                                    <div c:if test="${form.album.artist ne null}">
                                        <tr>
                                            <td class="h2">${form.album.artist}</td>
                                        </tr>
                                    </div>
                                    <div c:if test="${form.album.group ne null}">
                                        <tr>
                                            <td class="h2">${form.album.group}</td>
                                        </tr>
                                    </div>
                                    </tbody>
                                </table>

                                <table>
                                    <tbody>
                                    <tr>
                                        <td>
                                            <table>
                                                <tbody>
                                                <div>
                                                    <c:if test="${form.album.collection eq true}">
                                                        <tr>
                                                            <td>collectie</td>
                                                        </tr>
                                                    </c:if>
                                                </div>
                                                <tr>
                                                    <td class="with200">uitgebracht</td>
                                                    <td class="with350">${form.album.releaseYear}</td>
                                                </tr>
                                                <tr>
                                                    <td class="with200">media</td>
                                                    <td class="with350">${form.album.media}${form.recordsInBox}</td>
                                                </tr>
                                                <tr>
                                                    <td class="with200">label</td>
                                                    <td class="with350">${form.album.label}</td>
                                                </tr>
                                                <tr>
                                                    <td class="with200">Muziek&nbsp;soort</td>
                                                    <td class="with350">${form.album.style}</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="with350" >
                                            <a href="#"
                                               onclick="window.open('/album/images/${form.album.id}'
                                                       ,'Images'
                                                       ,'location=0, width=910, height=610');
                                                       return false;">
                                                <img src="<c:url value="/image/${form.album.imageIds[0]}"/> " width="300"
                                                     height="300"/>
                                            </a>
                                        </td>
                                    </tr>
                                     <tr>
                                         <td>
                                            <a href="/album/backwards/${form.album.id}" >vorige</a>
                                            <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
                                            <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
                                            <span>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp</span>
                                            <a href="/album/forwards/${form.album.id}" >volgende</a>
                                         </td>
                                     </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="with700">
                                <jsp:include page="record_list.jsp"/>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <spring:url
            value="/record/addRecord/${form.album.id}/${form.album.totalRecords}/${form.album.media}/${form.album.collection}"
            var="addURL"/>
    <input type="submit" class="btn tdbutton marginleft20" formaction="${addURL}" value="Record/Nummer Toevoegen"/>
    <spring:url
            value="/album/allAlbums/"
            var="backURL"/>
    <input type="submit" class="btn tdbutton marginleft20" formaction="${backURL}" value="Overzicht"/>

</form>
<jsp:include page="footer.jsp"/>

</body>
</html>