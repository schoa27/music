<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>RecordList</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<form>
    <table class="listtable">
        <c:forEach items="${form.records}" var="record">
            <tr>
                <th class="headfont25">
                    <span>Record&nbsp;${record.number}</span>
                </th>
            </tr>
            <tr>
                <td>
                    <table class="listtable marginleft20 maeginright20 marginbottom25">
                        <thead>
                        <th class="height50 headfont20 centeritem" scope="row">Tietel</th>
                        <th class="height50 headfont20 centeritem" scope="row">Duur</th>
                        <c:if test="${form.side}">
                            <th class="height50 headfont20 centeritem" scope="row">Zijde</th>
                        </c:if>
                        </thead>
                        <tbody>
                        <c:forEach items="${record.trackList}" var="track">
                            <td class="with350">${track.title}</td>
                            <td class="with100 centeritem">${track.time}</td>
                            <c:if test="${form.side}">
                                <td class="with100 centeritem">${track.side}</td>
                            </c:if>
                            <c:if test="${!form.albumform}">
                                <td>
                                    <spring:url value="/track/updateTrack/${form.albumId}/${track.id}" var="updateURL"/>
                                    <input type="submit" class="btn tdbutton" formaction="${updateURL}"
                                           value="Wijzig"/>
                                </td>
                                <td>
                                    <spring:url value="/track/deleteTrack/${form.albumId}/${track.id}" var="deleteURL"/>
                                    <input type="submit" class="btn tdbutton" formaction="${deleteURL}"
                                           value="Verwijder"/>
                                </td>
                            </c:if>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </td>
            </tr>
        </c:forEach>
    </table>
</form>
</body>
</html>