<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Group Form</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">


<spring:url value="/group/save" var="saveURL"/>
<spring:url value="/group/allGroups" var="cancelURL"/>
<table class="maintable">
    <tr>
        <td>
            <jsp:include page="title.jsp"/>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset class="marginleft20 marginright20">
                <legend>
                <c:choose>
                    <c:when test="${groupForm.updateGroup}" >
                            <h2 class="h2">Groep Wijzigen</h2>
                    </c:when>
                    <c:otherwise>
                            <h2 class="h2">Groep Toevoegen</h2>
                    </c:otherwise>
                </c:choose>
                </legend>
                <form:form modelAttribute="groupForm" method="post" enctype="multipart/form-data" action="${saveURL}" cssClass="form">
                <form:hidden path="group.id"/>

                <table class="maintable marginleft20">
                    <tr>
                        <td>
                            <label>Groepnaam</label>
                        </td>
                        <td>
                            <form:input path="group.name" cssClass="height40" id="name"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Afbeelding</label>
                        </td>
                        <td>
                            <form:input type="file" path="group.multipartFile" cssClass="btn tdbutton" id="image"/>
                        </td>
                    </tr>
                    <tr>
                      <c:if test="${!groupForm.delImage}">
                        <td>
                            <span>Bestaande afbeelding</span>
                            </br>
                            <span>verwijderen</span>
                        </td>
                        <td>
                            <spring:url value="/group/image/delete/${groupForm.group.id}" var="deleteURL" />
                            <input type="submit" formaction="${deleteURL}" value="Verwijder" class="btn tdbutton"/>
                        </td>
                    </c:if>
                    </tr>
                </table>
            </fieldset>
            <button type="submit" class="btn tdbutton marginleft20 margintop25">Opslaan</button>
            <spring:url value="/group/allGroups" var="cancelURL"/>
            <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
            </form:form>
        </td>
    </tr>
</table>
<jsp:include page="footer.jsp"/>

</body>
</html>
