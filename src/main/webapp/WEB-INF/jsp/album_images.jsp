<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>AlbumImages</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
    <link rel="stylesheet" href="/css/images.css">
    <script src="/js/viewFunctions.js"></script>
</head>

<body class="bodybackgroud">
<table class="imagetable">
    <tbody>
    <tr>
        <c:forEach var="image" items="${form.album.imageIds}">
            <td>
                <img src="<c:url value="/image/${image}"/> "
                     class="zoom"/>
            </td>
        </c:forEach>
    </tr>
    </tbody>
</table>
<br/>
<input type="button" class="btn tdbutton marginleft20" onclick="closeWin()" value="Sluiten"/>
</body>
</html>
