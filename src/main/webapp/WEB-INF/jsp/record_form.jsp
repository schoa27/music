<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Add Record</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
    <script src="/js/tableFunctions.js"></script>
</head>

<body class="bodybackgroud">

<spring:url value="/record/saveTrack" var="saveURL"/>

<table class="maintable">
    <tr>
        <td>
            <jsp:include page="title.jsp"/>
            <jsp:include page="main_menu.jsp"/>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset>

                <legend>
                    <h2 class="h2">Record Toevoegen</h2>
                </legend>

                <form:form modelAttribute="form" method="get" action="${saveURL}">
                    <table>
                        <tr>
                            <td>
                                <form:hidden path="albumId" value="${form.albumId}"/>
                                <form:hidden path="collection" value="${form.collection}"/>
                                <form:hidden path="mediaType" value="${form.mediaType}"/>
                                <form:hidden path="recordCount" value="${form.recordCount}"/>
                                <form:hidden path="trackId" value="${form.trackId}"/>
                                <c:if test="${form.recordImage}">
                                 <table>
                                     <tr>
                                        <td class="with250">
                                            Record afbeelding
                                        </td>
                                        <td>
                                           <input id="recordImage" type="file" class="btn btninput" path="record.image" />
                                        </td>
                                     </tr>
                                 </table>
                                </c:if>
                                <table>

                                    <tr>
                                        <td class="with100">
                                            Record
                                        </td>
                                        <td class="with100">
                                            <form:select path="record.number" cssClass="btn tdbutton" id="number">
                                                <c:forEach items="${form.recordnumbers}" var="number">
                                                    <form:option value="${number}">
                                                        <c:out value="${number.intValue()}"/>
                                                    </form:option>
                                                </c:forEach>
                                            </form:select>
                                        </td>
                                        <c:if test="${form.side}">
                                        <td class="with100">
                                            kant
                                        </td>
                                            <td>
                                                <form:select path="record.side" cssClass="btn tdbutton" id="side">
                                                    <c:forEach items="${form.sides}" var="side">
                                                        <form:option value="${side}">
                                                            <c:out value="${side}"/>
                                                        </form:option>
                                                    </c:forEach>
                                                </form:select>
                                            </td>
                                        </c:if>
                                    </tr>
                                </table>
                                <br/>
                                <table>
                                    <tbody>
                                    <tr>
                                        <td class="with250">
                                            <label>Tietel nummer</label>
                                        </td>
                                        <td>
                                            <form:input path="record.title" id="title" cssClass="height40"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="with100">
                                            <label>Speeltijd (min:sec)</label>
                                        </td>
                                        <td>
                                            <form:input path="record.durationMin" id="duration" cssClass="height40 with30"/>
                                            <span>:</span>
                                            <form:input path="record.durationSec" id="duration" cssClass="height40 with30" />
                                        </td>
                                    </tr>
                                    <c:if test="${form.collection}">
                                        <tr>
                                            <td class="with100">
                                                <label>Artist</label>
                                            </td>
                                            <td>
                                                <form:input path="record.artist" id="duration" cssClass="height40"/>
                                            </td>
                                        </tr>
                                    </c:if>
                                    </tbody>
                                </table>

                                <button type="submit" class="btn tdbutton marginleft20 margintop25">Opslaan</button>
                                <spring:url value="/album/details/${form.albumId}" var="cancelURL" />
                                <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
                                <input type="submit" formaction="${cancelURL}" value="Klaar" class="btn tdbutton"/>
                            </td>
                            <td>
                                <jsp:include page="record_list.jsp"/>
                            </td>
                        </tr>
                    </table>
                </form:form>

            </fieldset>
        </td>
    </tr>
</table>

<jsp:include page="footer.jsp"/>

</body>
</html>