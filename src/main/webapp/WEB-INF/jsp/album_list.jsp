<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>AlbumList</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
    <script src="/js/viewFunctions.js"></script>
</head>

<body class="bodybackgroud">

<form>
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
                <jsp:include page="main_menu.jsp"/>
            </td>
        </tr>
        <tr>
            <td class="centeritem">
                <fieldset>
                    <legend>
                        <h2 class="h2">Overzicht Albums</h2>
                    </legend>

                    <legend>
                        <h2 class="h3">totaal&nbsp${form.albumList.size()}&nbspalbums</h2>
                    </legend>
                    <table class="listtable marginleft20 maeginright20 marginbottom25">
                        <thead>
                        <th class="headfont" scope="row">Afbeelding</th>
                        <spring:url value="/album/sort/title" var="sortURL"/>
                        <th class="with250 height50 headfont" scope="row">
                            <a href="${sortURL}">Tietel</a>
                        </th>
                        <spring:url value="/album/sort/artistGroup" var="sortURL"/>
                        <th class="with250 height50 headfont" scope="row">
                            <a href="${sortURL}">Artiest/Groep</a>
                        </th>
                        <spring:url value="/album/sort/releaseYear" var="sortURL"/>
                        <th class="with100 height50 headfont" scope="row">
                            <a href="${sortURL}">Uitgebracht</a>
                        </th>
                        <th class="with100 height50 headfont" scope="row">Media</th>
                        <th class="with250 height50 headfont" scope="row">Genre</th>
                        <th class="height50 headfont"></th>
                        <th class="height50 headfont"></th>
                        </thead>
                        <tbody>

                        <c:forEach items="${form.albumList}" var="album">
                            <tr class="trcolor cursors"
                                onclick='detailData("/album/details/" + ${album.id} )'>
                                <td>
                                    <img src="<c:url value="/image/${album.imageIds[0]}"/> " width="50" , height="50"/>
                                </td>
                                <td>${album.title}</td>

                                <div>
                                    <c:if test="${album.collection eq true}">
                                        <td>collection</td>
                                    </c:if>
                                </div>
                                <div>
                                    <c:if test="${album.collection eq false}">
                                        <div>
                                            <c:if test="${album.artist != null}">
                                                <td>${album.artist}</td>
                                            </c:if>
                                        </div>
                                        <div>
                                            <c:if test="${album.group != null}">
                                                <td>${album.group}</td>
                                            </c:if>
                                        </div>
                                    </c:if>
                                </div>

                                <td>${album.releaseYear}</td>
                                <td>${album.media}</td>
                                <td>${album.style}</td>
                                <td>
                                    <spring:url value="/album/updateAlbum/${album.id}" var="updateURL"/>
                                    <input type="submit" class="btn tdbutton" formaction="${updateURL}" value="Wijzig"/>
                                </td>
                                <td>
                                    <spring:url value="/album/deleteAlbum/${album.id}" var="deleteURL"/>
                                    <input type="submit" class="btn tdbutton" formaction="${deleteURL}"
                                           value="Verwijder"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <spring:url value="/album/addAlbum" var="addURL"/>
    <spring:url value="/music" var="mainURL"/>
    <input type="submit" class="btn tdbutton marginleft20" formaction="${addURL}" value="Toevoegen"/>
    <input type="submit" class="btn tdbutton marginleft20" formaction="${mainURL}" value="Hoofdmenu"/>
    <jsp:include page="footer.jsp"/>
</form>
</body>
</html>