<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>MainMenu</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/menu.css"/>
</head>

<body>

<ul id="nav-menu">
    <li><a href="#">Overzicht</a><span class="darrow">&#9660;</span>
        <ul class="sub1">
            <li>
                <spring:url value="/album/allAlbums" var="allAlbumURL"/>
                <a href="${allAlbumURL}" role="button">Albums</a>
            </li>
            <li>
                <spring:url value="/artist/allArtists" var="allArtistURL"/>
                <a href="${allArtistURL}" role="button">Artiesten</a>
            </li>
            <li>
                <spring:url value="/group/allGroups" var="allGroupURL"/>
                <a href="${allGroupURL}" role="button">Groepen</a>
            </li>
        </ul>
    </li>
    <li><a href="#">Zoeken</a><span class="darrow">&#9660;</span>
        <ul class="sub1">
            <li>
                <spring:url value="/artist/search" var="searchArtistURL"/>
                <a href="${searchArtistURL}" role="button">Artiest</a>
            </li>
            <li>
                <spring:url value="/group/search" var="searchGroupURL"/>
                <a href="${searchGroupURL}" role="button">Groep</a>
            </li>
            <li>
                <spring:url value="/album/search" var="searchAlbumURL"/>
                <a href="${searchAlbumURL}" role="button">Album</a>
            </li>
        </ul>
    </li>
    <li>
        <spring:url value="/setup" var="allSetupURL"/>
        <a href="${allSetupURL}" role="button">Instellingen</a>
    </li>
    <li><a href="#">Beheer</a><span class="darrow">&#9660;</span>
        <ul class="sub1">
            <li>
                <spring:url value="/beheer/export" var="beheerExportURL"/>
                <a href="${beheerExportURL}" role="button">Export gegevens</a>
            </li>
            <li>
                <spring:url value="/beheer/import" var="beheerImportURL"/>
                <a href="${beheerImportURL}" role="button">Import gegevens</a>
            </li>
        </ul>
    </li>
</ul>

</body>
</html>