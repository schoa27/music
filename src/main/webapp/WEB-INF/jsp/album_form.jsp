<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Album Form</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
    <script src="/js/viewFunctions.js"></script>
    <script src="/js/jquery-3.3.1.js"></script>
    <script src="/js/showHideFunctions.js"></script>
</head>

<body class="bodybackgroud">

<spring:url value="/album/saveAlbum" var="saveURL"/>
<spring:url value="/album/allAlbums" var="cancelURL"/>
<table class="maintable">
    <tr>
        <td>
            <jsp:include page="title.jsp"/>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset class="marginleft20 marginright20">
                <legend>
                    <c:choose>
                        <c:when test="${form.addAlbum}" >
                            <h2 class="h2">Album Toevoegen</h2>
                        </c:when>
                        <c:otherwise>
                            <h2 class="h2">Album Wijzigen</h2>
                        </c:otherwise>
                    </c:choose>
                </legend>
                <form:form modelAttribute="form" method="post" action="${saveURL}" enctype="multipart/form-data" >
                <form:hidden path="album.id"/>
                <table class="maintable marginleft20">
                    <tr>
                        <td>
                            <label>Tietel</label>
                        </td>
                        <td>
                            <form:input path="album.title" id="title" cssClass="height40"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Uitgebracht (jaar)</label>
                        </td>
                        <td>
                            <form:input path="album.releaseYear" id="releaseYear" cssClass="height40"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Verzamel</label>
                        </td>
                        <td>
                            <form:checkbox path="album.collection" id="col-check" onchange="checkCollection(this)" cssClass="height40"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Album label</label>
                        </td>
                        <td>
                            <form:input path="album.label" id="label" cssClass="height40"/>
                        </td>
                    </tr>
                    <tr id="artist">
                        <td>
                            <div id="collectiona">
                                <div id="artist-label">
                                    <label>Artiest</label>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div id="artist-select">
                                <form:select path="album.artist" cssClass="btn tdbutton" id="hide-group">

                                    <c:choose>
                                        <c:when test="${empty form.album.artist}" >
                                            <form:option value="NONE" label="--- Kies ---"/>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option value="${form.album.artistId}" label="${form.album.artist}"/>
                                        </c:otherwise>
                                    </c:choose>

                                    <c:forEach items="${form.artists}" var="artist">
                                        <form:option value="${artist.id}">
                                            <c:out value="${artist.firstname} ${artist.lastname}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                                <input type="submit" id="show-group" formaction="" value="Herstel Groep"
                                       class="btn tdbutton"/>
                            </div>
                        </td>
                    </tr>
                    <tr id="group">
                        <td>
                            <div id="group-label">
                                <label>Groep</label>
                            </div>
                        </td>
                        <td>
                            <div id="group-select">
                                <form:select path="album.group" cssClass="btn tdbutton" id="hide-artist">

                                    <c:choose>
                                        <c:when test="${empty form.album.group}" >
                                            <form:option value="NONE" label="--- Kies ---"/>
                                        </c:when>
                                        <c:otherwise>
                                            <form:option value="${form.album.groupId}" label="${form.album.group}"/>
                                        </c:otherwise>
                                    </c:choose>

                                    <c:forEach items="${form.groups}" var="group">
                                        <form:option value="${group.id}">
                                            <c:out value="${group.name}"/>
                                        </form:option>
                                    </c:forEach>
                                </form:select>
                                <input type="submit" id="show-artist" formaction="" value="Herstel Artiest"
                                       class="btn tdbutton"/>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Genre</label>
                        </td>
                        <td>
                            <form:select path="album.style" cssClass="btn tdbutton" id="style">

                                <c:choose>
                                    <c:when test="${empty form.album.style}" >
                                        <form:option value="NONE" label="--- Kies ---"/>
                                    </c:when>
                                    <c:otherwise>
                                        <form:option value="${form.album.styleType}" label="${form.album.style}"/>
                                    </c:otherwise>
                                </c:choose>

                                <c:forEach items="${form.styles}" var="style">
                                    <form:option value="${style}">
                                        <c:out value="${style.style}"/>
                                    </form:option>
                                </c:forEach>
                            </form:select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Media</label>
                        </td>
                        <td>
                            <form:select path="album.media" cssClass="btn tdbutton" onchange="checkItem(this)"
                                         id="media" >

                                <c:choose>
                                    <c:when test="${empty form.album.media}" >
                                        <form:option value="NONE" label="--- Kies ---"/>
                                    </c:when>
                                    <c:otherwise>
                                        <form:option value="${form.album.mediaType}" label="${form.album.media}"/>
                                    </c:otherwise>
                                </c:choose>

                                <c:forEach items="${form.mediaTypes}" var="media">
                                    <form:option value="${media}">
                                        <c:out value="${media.mediaType}"/>
                                    </form:option>
                                </c:forEach>
                            </form:select>
                            <span id="media-select">
                                <label>Aantal records</label>
                                <form:input path="album.totalRecords" id="totalRecords"
                                            type="number" value="0" cssClass="height40 with50"/>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Afbeelding</label>
                        </td>
                        <td>
                            <form:input type="file" name="files" accept=".png, .jpeg, .jpg" multiple="multiple" path="album.multipartFiles" cssClass="btn tdbutton height40" id="image"/>
                        </td>
                    </tr>
                    <tr>
                        <c:if test="${!form.delImage}">
                        <td>
                            <span>Bestaande afbeelding(en)</span>
                            </br>
                            <span>verwijderen</span>
                        </td>
                        <td>
                           <spring:url value="/album/image/delete/${form.album.id}" var="deleteURL" />
                           <input type="submit" formaction="${deleteURL}" value="Verwijder" class="btn tdbutton"/>
                        </td>
                    </c:if>
                    </tr>
                </table>
            </fieldset>
            <button type="submit" class="btn tdbutton marginleft20 margintop25">Opslaan</button>
            <spring:url value="/album/allAlbums" var="cancelURL" />
            <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
            </form:form>
        </td>
    </tr>
</table>
<jsp:include page="footer.jsp"/>

</body>
</html>
