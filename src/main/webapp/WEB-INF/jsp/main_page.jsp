<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <meta http-equiv="refresh" content="20">
    <title>MyMusic</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<form>
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
                <jsp:include page="main_menu.jsp"/>
            </td>
        </tr>
    <tr>
    <td>
        <div class="centeritem">
                <br/>
                    <span><b>Huidige instellingen</b></span>
                <br/>
                    <span><b>lokatie waar de afbeeldingen staan.</b></span>
                <br/>
                    <span><b>${imagepath}</b></span>
        </div>
    </td>
    </tr>
        <tr>
            <td>
                <div class="centeritem">
                      <c:if test="${imageId[0] > 0}">
                          <img src="<c:url value="/image/${imageId[0]}"/> " width="500"
                            height="500"/>
                      </c:if>
                </div>
            </td>
        </tr>
    </table>
</form>
<jsp:include page="footer.jsp"/>

</body>
</html>