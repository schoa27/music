<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>ArtistList</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<form>
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
                <jsp:include page="main_menu.jsp"/>
            </td>
        </tr>
        <tr>
            <td class="centeritem">
                <fieldset>
                    <legend>
                        <h2 class="h2">Overzicht Artiesten</h2>
                    </legend>
                    <table class="listtable marginleft20 marginright20 marginbottom25" >
                        <thead>
                        <th class="headfont" scope="row">Afbeelding</th>
                        <spring:url value="/artist/sort/firstname" var="sortURL"/>
                        <th class="with250 height50 headfont" scope="row">
                            <a href="${sortURL}">Voornaam</a>
                        </th>
                        <spring:url value="/artist/sort/lastname" var="sortURL"/>
                        <th class="with250 height50 headfont" scope="row">
                            <a href="${sortURL}">Achternaam</a>
                        </th>
                        <th class="height50 headfont" scope="row"/>
                        <th class="height50 headfont" scope="row"/>
                        </thead>
                        <tbody>
                        <c:forEach items="${artistForm.artistsList}" var="artist">
                            <tr class="trcolor">
                                <td>
                                    <img src="<c:url value="/image/${artist.imageId}"/> " width="50" , height="50"/>
                                </td>
                                <td>${artist.firstname}</td>
                                <td>${artist.lastname}</td>
                                <td>
                                    <spring:url value="/artist/updateArtist/${artist.id}" var="updateURL"/>
                                    <input type="submit" formaction="${updateURL}" value="Wijzig" class="btn tdbutton"/>
                                </td>
                                <td>
                                    <spring:url value="/artist/deleteArtist/${artist.id}" var="deleteURL"/>
                                    <input type="submit" formaction="${deleteURL}" value="Verwijder" class="btn tdbutton"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <spring:url value="/artist/addArtist" var="addURL"/>
    <spring:url value="/music" var="mainURL"/>
    <input type="submit" formaction="${addURL}" value="Toevoegen" class="btn tdbutton marginleft20"/>
    <input type="submit" class="btn tdbutton marginleft20" formaction="${mainURL}" value="Hoofdmenu"/>
    <jsp:include page="footer.jsp"/>
</form>

</body>
</html>