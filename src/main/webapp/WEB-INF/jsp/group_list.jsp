<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>GroupList</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<form>
    <table class="maintable">
        <tr>
            <td>
                <jsp:include page="title.jsp"/>
                <jsp:include page="main_menu.jsp"/>
            </td>
        </tr>
        <tr>
            <td class="centeritem">
                <fieldset>
                    <legend>
                        <h2 class="h2">Overzicht Groepen</h2>
                    </legend>
                    <table class="listtable marginleft20 marginright20 marginbottom25">
                        <thead>
                        <th class="headfont" scope="row">Afbeelding</th>
                        <spring:url value="/group/sort/name" var="sortURL"/>
                        <th class="with350 height50 headfont" scope="row">
                            <a href="${sortURL}">Naam</a>
                        </th>
                        <th class="height50 headfont" scope="row"/>
                        <th class="height50 headfont" scope="row"/>
                        </thead>
                        <tbody>
                        <c:forEach items="${groupForm.groupsList}" var="group">
                            <tr class="trcolor">
                                <td>
                                    <img src="<c:url value="/image/${group.imageId}"/> " width="50" , height="50"/>
                                </td>
                                <td>${group.name}</td>
                                <td>
                                    <spring:url value="/group/updateGroup/${group.id}" var="updateURL"/>
                                    <input type="submit" formaction="${updateURL}" value="Wijzig" class="btn tdbutton"/>
                                </td>
                                <td>
                                    <spring:url value="/group/deleteGroup/${group.id}" var="deleteURL"/>
                                    <input type="submit" formaction="${deleteURL}" value="Verwijder"
                                           class="btn tdbutton"/>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </fieldset>
            </td>
        </tr>
    </table>
    <spring:url value="/group/addGroup" var="addURL"/>
    <spring:url value="/music" var="mainURL"/>
    <input type="submit" formaction="${addURL}" value="Toevoegen" class="btn tdbutton marginleft20"/>
    <input type="submit" class="btn tdbutton marginleft20" formaction="${mainURL}" value="Hoofdmenu"/>
</form>
<jsp:include page="footer.jsp"/>

</body>
</html>