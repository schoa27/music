<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Artist Search Form</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">


<table class="maintable">
    <tr>
        <td>
            <jsp:include page="title.jsp"/>
            <jsp:include page="main_menu.jsp"/>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset class="marginleft20 marginright20">
                <c:if test="${searchForm.searchItem == 'artist'}">
                    <spring:url value="/artist/searchitem" var="searchURL"/>
                    <legend>
                        <h2 class="h2">Artiest(en) Zoeken</h2>
                    </legend>
                </c:if>
                <c:if test="${searchForm.searchItem == 'group'}">
                    <spring:url value="/group/searchitem" var="searchURL"/>
                    <legend>
                        <h2 class="h2">Groep(en) Zoeken</h2>
                    </legend>
                </c:if>
                <c:if test="${searchForm.searchItem == 'album'}">
                    <spring:url value="/album/searchitem" var="searchURL"/>
                    <legend>
                        <h2 class="h2">Album(s) Zoeken</h2>
                    </legend>
                </c:if>
                <form:form modelAttribute="searchForm" method="post" action="${searchURL}">
                <table class="maintable marginleft20">
                    <tr>
                        <td>
                            <label>Zoek gegeven(s)</label>
                        </td>
                        <td>
                            <form:input path="searchFor" cssClass="height40" id="searchFor"/>
                            <form:errors path="searchFor" cssClass="error"/>
                        </td>
                    </tr>
                    <c:if test="${searchForm.searchItem == 'album'}">
                        <tr>
                            <td>
                                <label>Tietel</label>
                            </td>
                            <td>
                                <form:radiobutton path="choose" value="title" cssClass="height40" id="title" checked="checked"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Artiest</label>
                            </td>
                            <td>
                                <form:radiobutton path="choose" value="artist" cssClass="height40" id="artist"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Groep</label>
                            </td>
                            <td>
                                <form:radiobutton path="choose" value="group" cssClass="height40" id="group"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Uitgebracht (jaar)</label>
                            </td>
                            <td>
                                <form:radiobutton path="choose" value="year" cssClass="height40" id="year"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Media</label>
                            </td>
                            <td>
                                <form:radiobutton path="choose" value="media" cssClass="height40" id="media"/>
                                <div>
                                    <form:select path="media" cssClass="btn tdbutton" id="selectmedia">
                                        <c:forEach items="${searchForm.mediaTypes}" var="media">
                                            <form:option value="${media}">
                                                <c:out value="${media.mediaType}"/>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Genre</label>
                            </td>
                            <td>
                                <form:radiobutton path="choose" value="style" cssClass="height40" id="style"/>
                                <div>
                                    <form:select path="style" cssClass="btn tdbutton" id="selectstyle">
                                        <c:forEach items="${searchForm.styles}" var="style">
                                            <form:option value="${style}">
                                                <c:out value="${style.style}"/>
                                            </form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </td>
                        </tr>
                    </c:if>
                </table>
            </fieldset>
            <button type="submit" class="btn tdbutton margintop25 marginleft20">Zoeken</button>
            </form:form>
        </td>
    </tr>
</table>
<jsp:include page="footer.jsp"/>

</body>
</html>
