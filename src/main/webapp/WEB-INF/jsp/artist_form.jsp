<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Artist Form</title>
    <link rel="stylesheet" href="/css/buttons.css"/>
    <link rel="stylesheet" href="/css/page.css"/>
</head>

<body class="bodybackgroud">

<spring:url value="/artist/save" var="saveURL"/>
<spring:url value="/artist/allArtists" var="cancelURL"/>
<table class="maintable">
    <tr>
        <td>
            <jsp:include page="title.jsp"/>
        </td>
    </tr>
    <tr>
        <td>
            <fieldset class="marginleft20 marginright20">
                <legend>
                    <c:choose>
                         <c:when test="${artistForm.updateArtist}" >
                              <h2 class="h2">Artiest Wijzigen</h2>
                         </c:when>
                         <c:otherwise>
                              <h2 class="h2">Artiest Toevoegen</h2>
                         </c:otherwise>
                    </c:choose>
                </legend>
                <form:form modelAttribute="artistForm" method="post" enctype="multipart/form-data" action="${saveURL}">
                <form:hidden path="artist.id"/>
                <table class="maintable marginleft20">
                    <tr>
                        <td>
                            <label>Voornaam</label>
                        </td>
                        <td>
                            <form:input path="artist.firstname" cssClass="height40" id="firstname"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Achternaam</label>
                        </td>
                        <td>
                            <form:input path="artist.lastname" cssClass="height40" id="lastname"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Afbeelding</label>
                        </td>
                        <td>
                            <form:input type="file" path="artist.multipartFile" cssClass="btn tdbutton" id="image"/>
                        </td>
                    </tr>
                    <tr>
                         <c:if test="${!artistForm.delImage}">
                        <td>
                            <span>Bestaande afbeelding</span>
                            </br>
                            <span>verwijderen</span>
                        </td>
                        <td>
                            <spring:url value="/artist/image/delete/${artistForm.artist.id}" var="deleteURL" />
                            <input type="submit" formaction="${deleteURL}" value="Verwijder" class="btn tdbutton"/>
                        </td>
                    </c:if>
                    </tr>
                </table>
            </fieldset>
            <button type="submit" class="btn tdbutton margintop25 marginleft20">Opslaan</button>
            <spring:url value="/artist/allArtists" var="cancelURL"/>
            <input type="submit" formaction="${cancelURL}" value="Annuleer" class="btn tdbutton"/>
            </form:form>
        </td>
    </tr>
</table>
<jsp:include page="footer.jsp"/>

</body>
</html>
