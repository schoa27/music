function changeColor(tableRow, highLight) {
    if (highLight) {
        tableRow.style.backgroundColor = '#949494'; //'donkergrijs'
    } else {
        tableRow.style.backgroundColor = '#D3D3D3'; //'lightgrijs';
    }
}

function detailData(servlet) {
    document.location.href = servlet;
}

function checkItem(media) {
    var link = document.getElementById('media-select');

    if (media.value == ("MT10")
        || media.value == ("MT11")
        || media.value == ("MT12")
        || media.value == ("MT13")
        || media.value == ("MT14")
        || media.value == ("MT15")) {
        link.style.visibility = 'visible';
    } else {
        link.style.visibility = 'hidden';
    }
}

function checkCollection(collection) {
    var artist = document.getElementById('artist');
    var group = document.getElementById('group');

    if (collection.checked) {
        artist.style.visibility = 'hidden'
        group.style.visibility = 'hidden'
    } else {
        artist.style.visibility = 'visible';
        group.style.visibility = 'visible';
    }
}

function closeWin() {
   window.close();
}