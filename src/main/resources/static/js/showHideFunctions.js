$(document).ready(function () {
    $("#hide-group").click(function () {
        $("#group-label").hide();
        $("#group-select").hide();
    });

    $("#show-group").click(function () {
        $("#group-label").show();
        $("#group-select").show();
    });

    $("#hide-artist").click(function () {
        $("#artist-label").hide();
        $("#artist-select").hide();
    });

    $("#show-artist").click(function () {
        $("#artist-label").show();
        $("#artist-select").hide();
    });
});