package nl.scholtens.music.facade;

import nl.scholtens.music.From.RecordForm;
import nl.scholtens.music.domain.Album;
import nl.scholtens.music.domain.Record;
import nl.scholtens.music.domain.Track;
import nl.scholtens.music.dto.RecordDto;
import nl.scholtens.music.dto.TrackDto;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.service.AlbumService;
import nl.scholtens.music.service.RecordService;
import nl.scholtens.music.service.TrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RecordFacade {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private RecordService recordService;

    @Autowired
    private TrackService trackService;

    public RecordForm createRecordForm(Integer recordnumbers, Integer albumId) {
        RecordForm form = new RecordForm();
        createRecordNumbers(form.getRecordnumbers(), recordnumbers);
        createSideList(form.getSides());

        if (albumId != null) {
            List<Record> records = recordService.getRecordByAlbumId(albumId);
        form.setRecords(records.stream().map(record -> convertRecordToDto(record, 0)).collect(Collectors.toList()));
        }

        Album album = albumService.getAlbumById(albumId);
        form.setAlbumId(albumId);
        form.setMediaType(album.getMediaType().getMediaType());
        form.setCollection(album.isCollection());
        form.setRecordCount(recordnumbers);
        form.setSide(haveSides(album.getMediaType()));
//        form.setRecordImage(checkMediaType(album.getMediaType()));
        return form;
    }

    public RecordForm getRecordById(int albumId, int trackId) {
        Record record = recordService.getRecordById(trackId);

        RecordForm form = new RecordForm();
        RecordDto recordDto = convertRecordToDto(record, trackId);
        form.setAlbumId(albumId);
        form.setTrackId(trackId);
        form.setRecord(recordDto);
        form.getRecords().add(recordDto);

        for (int i = 1; i <= record.getNumber(); i++ ) {
         form.getRecordnumbers().add(new Integer(i));
        }

        if (!checkMediaType(record.getAlbum().getMediaType())) {
            form.setSides(new ArrayList<String>(Arrays.asList("A", "B")));
            form.setSide(true);
        }

        return form;
    }

    public void saveRecord(Integer albumId, RecordDto dto, Integer trackId) {
        dto.setSide(dto.getSide() == null ? "NONE" : dto.getSide());
        Record record = recordService.getRecordByAlbumIdAndRecordNumber(albumId, dto.getNumber());
        record = (record == null) ? createRecord(dto, new Record(), albumId) :  createRecord(dto, record, albumId);

        Track track = trackService.getTrackById(trackId);

        track = (track == null) ? createTrack(dto, record, new Track()) : createTrack(dto, record, track);

        trackService.saveTrack(track);
    }

    public Integer getNumberOfRecordsByAlbumId(Integer albumId) {
        return recordService.countRecords(albumId);
    }

    public void deleteTrack(Integer trackId) {
        Track track = trackService.getTrackById(trackId);
        trackService.deleteTrackById(trackId);

        if (trackService.countTrackByRecordId(track.getRecord().getId()) == 0) {
            recordService.deleteRecordById(track.getRecord().getId());
        }

    }


    private RecordDto convertRecordToDto(Record record,int trackId) {
        RecordDto dto = new RecordDto();
        dto.setNumber(record.getNumber());
        dto.setTrackList(record.getTracks().stream()
                .map(track -> convertTrackToDto(track))
                .collect(Collectors.toList()));

        if (trackId > 0) {
            Track track = record.getTracks().stream().filter(t -> t.getId() == trackId).findFirst().get();
            dto.setDuration(track.getTime());
            dto.setTitle(track.getTitle());
            dto.setSide(track.getSide());
        } else {
            dto.setTitle(record.getName());
        }

        return dto;
    }

    private TrackDto convertTrackToDto(Track track) {
        TrackDto dto = new TrackDto();
        dto.setId(track.getId());
        dto.setTime(track.getTime());
        dto.setTitle(track.getTitle());
        dto.setSide(track.getSide());
        return dto;
    }

    private Record createRecord(RecordDto dto, Record record, Integer albumId) {
        record.setAlbum(albumService.getAlbumById(albumId));
        record.setNumber(dto.getNumber());
        return record;
    }

    private Track createTrack(RecordDto dto, Record record, Track track) {
        track.setTitle(dto.getTitle());
        track.setTime(dto.getDuration());
        track.setSide(dto.getSide());
        track.setRecord(record);
        return track;
    }

    private void createRecordNumbers(List<Integer> list, Integer recordNumbers) {
        for (int i = 1; i <= recordNumbers; i++) {
            list.add(new Integer(i));
        }
    }

    private void createSideList(List<String> sides) {
        sides.add("A");
        sides.add("B");
    }

    private boolean haveSides(MediaType mediaType) {
        return !Arrays.asList(Constant.getMediaTypes()).contains(mediaType.name());
    }

    private boolean checkMediaType(MediaType mediaType) {
        if (mediaType.equals(MediaType.MT10) ||
                mediaType.equals(MediaType.MT11) ||
                mediaType.equals(MediaType.MT12) ||
                mediaType.equals(MediaType.MT13) ||
                mediaType.equals(MediaType.MT14)) {
            return true;
        }
        return false;
    }
}
