package nl.scholtens.music.facade;

import nl.scholtens.music.From.GroupForm;
import nl.scholtens.music.domain.Group;
import nl.scholtens.music.domain.Image;
import nl.scholtens.music.dto.GroupDto;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.service.FilesService;
import nl.scholtens.music.service.GroupService;
import nl.scholtens.music.service.SetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

@Component
public class GroupFacade {

    @Autowired
    private GroupService groupService;

    @Autowired
    private SetupService setupService;

    @Autowired
    private FilesService filesService;

    public GroupForm getAllGroups() {
        GroupForm form = new GroupForm();
        form.setGroupsList(createListOfGroups(groupService.getAllGroups()));
        return form;
    }

    public GroupForm getAllGroupsSorted(String sortItem, String sortType) {
        GroupForm form = new GroupForm();
        form.setGroupsList(createListOfGroups(groupService.getAllGroupsSorted(sortItem, sortType)));
        return form;
    }

    public GroupForm getAllGroupsByNameSorted(String sortItem, String sortType, String searchFor) {
        GroupForm form = new GroupForm();
        form.setGroupsList(createListOfGroups(groupService.findGroupByNameSorted(sortItem, sortType, searchFor)));
        return form;
    }

    public GroupForm getGroupById(Integer id, boolean updateGroup) {
        GroupForm form = new GroupForm();
        form.setUpdateGroup(updateGroup);
        form.setGroup(convertToGroupDto(groupService.getGroupById(id)));
        return form;
    }

    public GroupForm findGroupByName(String name) {
        GroupForm form = new GroupForm();
        form.setGroupsList(createListOfGroups(groupService.findGroupByName(name)));
        return form;
    }

    public void saveGroup(GroupDto dto) {
        Group group = groupService.getGroupById(dto.getId());
        if (group == null) {
            groupService.saveGroup(convertToGroup(dto, new Group(), false));
        } else {
            groupService.saveGroup(convertToGroup(dto, group, true));
        }
        filesService.uploadFile(dto.getMultipartFile(), Constant.IMAGE_PATH);
    }

    public void deleteGroup(Integer id) {
        groupService.deleteGroup(groupService.getGroupById(id));
    }

    public void deleteImages(int groupId) {
        groupService.delImage(groupId);
    }

    private List<GroupDto> createListOfGroups(List<Group> allGroups) {
        List<GroupDto> list = new ArrayList<>();
        allGroups.stream()
                .forEach(group -> list.add(convertToGroupDto(group)));
        return list;
    }

    private Group convertToGroup(GroupDto dto, Group group, boolean exist) {
        group.setId(dto.getId());
        group.setName(dto.getName());
        if (exist && group.getImage() !=null) {
            group.getImage().setImageName(dto.getMultipartFile().getOriginalFilename());
        } else {
            group.setImage(new Image(dto.getMultipartFile().getOriginalFilename(), group));
        }
        return group;
    }

    private GroupDto convertToGroupDto(Group group) {
        GroupDto dto = new GroupDto();
        dto.setId(group.getId());
        dto.setName(group.getName());
        if (group.getImage() != null) {
            dto.setImageId(group.getImage().getId());
        }
        return dto;
    }
}
