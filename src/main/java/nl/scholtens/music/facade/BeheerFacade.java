package nl.scholtens.music.facade;

import nl.scholtens.music.From.BeheerForm;
import nl.scholtens.music.domain.Album;
import nl.scholtens.music.domain.Artist;
import nl.scholtens.music.domain.Group;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.mappers.AlbumToXmlMapper;
import nl.scholtens.music.mappers.XmlToAlbumMapper;
import nl.scholtens.music.parsers.XmlParser;
import nl.scholtens.music.service.*;
import nl.scholtens.music.xml.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


@Component
public class BeheerFacade {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private ArtistService artistService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private FilesService filesService;


    public BeheerForm createExportForm() {
        BeheerForm form = new BeheerForm(false, true);
        form.setAlbums(albumService.getAllAlbums()
                .stream()
                .map(Album::getTitle)
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toList()));
        form.setArtists(artistService.getAllArtists()
                .stream()
                .map(artist -> new String(artist.getFirstname() + " " + artist.getLastname()))
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toList()));
        form.setGroups(groupService.getAllGroups()
                .stream()
                .map(Group::getName)
                .sorted(String::compareToIgnoreCase)
                .collect(Collectors.toList()));
        form.setMediaTypes(Arrays.asList(MediaType.values())
                .stream()
                .map(MediaType::getMediaType)
                .collect(Collectors.toList()));
        form.setFieldset("");
        return form;
    }

    public BeheerForm createImportForm() {
        BeheerForm form = new BeheerForm(true, false);
        return form;
    }

    public int exportCriteria(BeheerForm form) {
             if (form.isAll() && form.getAllMedia().equals(Constant.ALL)) {
            return exportAllAlbums();
        }

        if (!form.getArtist().equals(Constant.NONE)) {
            return exportAllAlbumsOfArtist(form.getArtist());

        }

        if (!form.getGroup().equals(Constant.NONE)) {
            return exportAllAlbumsOfGroup(form.getGroup());

        }

        if (!form.getAlbum().equals(Constant.NONE)
                && !form.getMedia().equals(Constant.NONE)) {
            return exportAnAlbum(form.getAlbum(), form.getMedia());
        }

        if (form.isAll() && !form.getAllMedia().equals(Constant.NONE)) {
            return exportAllAlbumsBtMediaType(form.getAllMedia());
        }
        return 0;
    }

    public int importCriteria(BeheerForm form) {
        final int[] counter = {0};

        if (form.getXmlMultipartFiles().length != 0) {
            Arrays.asList(form.getXmlMultipartFiles()).stream()
                    .forEach(xmlFile -> filesService.uploadFile(xmlFile, Constant.XML_PATH));
        }

        List<String> xmlFiles = Arrays.stream(form.getXmlMultipartFiles())
                .map(MultipartFile::getOriginalFilename)
                .collect(Collectors.toList());

        List<AlbumXML> albumXMLList = xmlFiles.stream()
                .map(s -> XmlParser.parsFromXML(s))
                .collect(Collectors.toList());

        albumXMLList.stream()
                .map(album -> new XmlToAlbumMapper(album).getAlbum())
                .collect(Collectors.toList())
                .stream()
                .peek(album -> counter[0]++)
                .forEach(album -> saveAlbum(album));

        if (form.getImgMultipartFiles().length !=0) {
            Arrays.asList(form.getImgMultipartFiles()).stream()
                    .forEach(imageFile -> filesService.uploadFile(imageFile, Constant.IMAGE_PATH));
        }

        return counter[0];
    }

    private void saveAlbum(Album album) {
        List<Album> albums = albumService.findAlbumByTitle(album.getTitle()).stream()
                .filter(o -> o.getMediaType().equals(album.getMediaType()))
                .collect(Collectors.toList());

        if (albums.isEmpty()) {
            Artist artist = album.getArtist() != null ? lookForArtist(album.getArtist()) : null;

            Group group = album.getGroup() != null ? lookForGroup(album.getGroup()) : null;

            if (group == null && artist == null ) {
                albumService.saveAlbum(album);
            } else {
                album.setArtist(artist);
                album.setGroup(group);
                albumService.saveAlbum(album);
            }
        } else {
            Album albumByTitle = albums.stream().filter(o -> o.getTitle().equals(album.getTitle())).findFirst().get();
            albumService.updateAlbum(albumByTitle);
        }
    }

    private Artist lookForArtist(Artist artist) {
        Artist artistByName = artistService.getAllArtists().stream()
                .filter(o -> o.getFirstname().equals(artist.getFirstname())
                             && o.getLastname().equals(artist.getLastname()))
                .findFirst()
                .orElse(null);

        return artistByName;
    }

    private Group lookForGroup(Group group) {
        Group groupByName = groupService.getAllGroups().stream()
                .filter(o -> o.getName().equals(group.getName()))
                .findFirst()
                .orElse(null);
        return groupByName;
    }

    private int exportAllAlbumsBtMediaType(String media) {
        List<Album> albumlist = albumService.getAllAlbums();
        if (!albumlist.isEmpty()) {
            albumlist.stream()
                    .filter(album -> album.getMediaType().getMediaType().equals(media))
                    .collect(Collectors.toList())
                    .stream()
                    .forEach(filterAlbum -> XmlParser.parsToXML(filterAlbum));
        }
        return albumlist.size();
    }

    private int exportAnAlbum(String albumTitle, String media) {
        List<Album> albumlist = albumService.findAlbumByTitle(albumTitle);
        Album result = albumlist.stream().filter(album -> album.getMediaType().getMediaType().equals(media))
                .findFirst()
                .orElse(null);

        if (result != null) {
            XmlParser.parsToXML(result);
        }
        return albumlist.size();
    }

    private int exportAllAlbumsOfGroup(String group) {
        List<Album> albumList = albumService.findAlbumByGroup(group);
        albumList.stream().forEach(album -> XmlParser.parsToXML(album));
        return albumList.size();
    }

    private int exportAllAlbumsOfArtist(String artist) {
        List<Album> albumList = albumService.getAllAlbumsOfArtist(artist);
        albumList.stream().forEach(album -> XmlParser.parsToXML(album));
        return albumList.size();
    }

    private int exportAllAlbums() {
        List<Album> albumList = albumService.getAllAlbums();
        albumList.stream().forEach(album -> XmlParser.parsToXML(album));
        return albumList.size();
    }
}
