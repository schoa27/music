package nl.scholtens.music.facade;

import nl.scholtens.music.From.AlbumForm;
import nl.scholtens.music.domain.Album;
import nl.scholtens.music.domain.Image;
import nl.scholtens.music.domain.Record;
import nl.scholtens.music.domain.Track;
import nl.scholtens.music.dto.AlbumDto;
import nl.scholtens.music.dto.RecordDto;
import nl.scholtens.music.dto.TrackDto;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;
import nl.scholtens.music.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Component
public class AlbumFacade {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private GroupService groupService;

    @Autowired
    private ArtistService artistService;

    @Autowired
    private RecordService recordService;

    @Autowired
    private SetupService setupService;

    @Autowired
    private FilesService filesService;

    public AlbumForm getAllAlbums() {
        AlbumForm form = new AlbumForm();

        List<Album> allAlbums = albumService.getAllAlbums();

        allAlbums.stream()
                .forEach(album -> sortedImagesNames(album));
        form.setAlbumList(creatListOfAlbums(allAlbums));
        return form;
    }

    public AlbumForm getAllAlbumsSorted(String sortItem, String sortType) {
        AlbumForm form = new AlbumForm();

        List<Album> albumList = albumService.getAllAlbumsSorted(sortItem, sortType);

        albumList.stream().forEach(album -> sortedImagesNames(album));
        form.setAlbumList(creatListOfAlbums(albumList));
        return form;
    }

    public AlbumForm getAllAlbumsSorted(String sortItem, Map<String, String> sortorder, String searchItem, String searchFor) {
        AlbumForm form = new AlbumForm();
        switch (searchItem) {
            case "title":
                return createAlbumForm(albumService.getAllAlbumSearchByTitleSorted(sortItem, sortorder.get(sortItem), searchFor));
            case "artist":
                return createAlbumForm(albumService.getAllAlbumSearchByArtistSorted(sortItem, sortorder.get(sortItem), searchFor));
            case "group":
                return createAlbumForm(albumService.getAllAlbumSearchByGrouptSorted(sortItem, sortorder.get(sortItem), searchFor));
            case "releaseYear":
                return createAlbumForm(albumService.getAllAlbumSearchByReleaseYearSorted(sortItem, sortorder.get(sortItem), searchFor));
            case "media":
                return createAlbumForm(albumService.getAllAlbumSearchByMediaTypetSorted(sortItem, sortorder.get(sortItem), searchFor));
            case "style":
                return createAlbumForm(albumService.getAllAlbumSearchByStyleSorted(sortItem, sortorder.get(sortItem), searchFor));
        }
        return form;
    }

    public AlbumForm getAlbumById(Integer id, boolean addAlbum) {
        AlbumForm form = createBasicForm(new AlbumForm());
        form.setAddAlbum(addAlbum);
         if (!addAlbum) {
             Album album = albumService.getAlbumById(id);
             sortedImagesNames(album);
             form.setAlbum(convertToAlbumDto(album));
         }

        if (form.getAlbum() != null && form.getAlbum().getTotalRecords() > 2) {
            form.setRecordsInBox(String.valueOf(", " + form.getAlbum().getTotalRecords()) + " records");
        }
        List<Record> recordList = recordService.getRecordByAlbumId(id);
        form.setRecords(convertToRecordAndSongDto(recordList));
        form.setSide(true);
        return form;
    }

    public AlbumForm getAlbumByArtist(String searchString) {
        return createAlbumForm(albumService.findAlbumByArtist(searchString));
    }

    public AlbumForm getAlbumByGroup(String searchString) {
        return createAlbumForm(albumService.findAlbumByGroup(searchString));
    }

    public AlbumForm getAlbumByReleaseYear(String year) {
        return createAlbumForm(albumService.findAlbumByReleaseYear(year));
    }

    public AlbumForm getAlbumByTitle(String searchString) {
        return createAlbumForm(albumService.findAlbumByTitle(searchString));
    }

    public AlbumForm getAlbumByMediaType(String type) {
        MediaType mediaType = Arrays.asList(MediaType.values()).stream()
                .filter(m -> m.name().equals(type))
                .findFirst()
                .orElse(null);
        return createAlbumForm(albumService.findAlbumsByMedia(mediaType));
    }

    public AlbumForm getAlbumByStyle(String style) {
        Style listStyle = Arrays.asList(Style.values()).stream()
                .filter(s -> s.name().equals(style))
                .findFirst()
                .orElse(null);
        return createAlbumForm(albumService.findAlbumsByStyle(listStyle));
    }

    public void saveAlbum(AlbumDto dto) {
        Album album = albumService.getAlbumById(dto.getId());
        if (album == null) {
            albumService.saveAlbum(convertToAlbum(new Album(), dto));
        } else {
            albumService.saveAlbum(convertToAlbum(album, dto));
        }
        String[] filePath = setupService.readSetupFile();


        if (dto.getMultipartFiles().length != 0) {
            Arrays.asList(dto.getMultipartFiles())
                    .stream()
                    .forEach(imageFile -> filesService.uploadFile(imageFile, Constant.IMAGE_PATH));
        }
    }

    public int bepaalNewAlbumId(Integer id, boolean operator) {
        List<Integer> albumIds = albumService.getAllAlbumIds();

        for (int i = 0; i < albumIds.size(); ++i) {
            if (albumIds.get(i).equals(id) ) {
                if (operator) {
                    if ( (i + 1) >= albumIds.size()) {
                        return albumIds.get(0);
                    } else {
                        return albumIds.get(i + 1);
                    }
                } else {
                    if( (i - 1) < 0) {
                        return albumIds.get(albumIds.size() - 1);
                    } else {
                        return albumIds.get(i - 1);
                    }
                }
            }
        }
        return 0;
    }

    public void deleteAlbum(Integer id) {
        albumService.deleteAlbum(albumService.getAlbumById(id));
    }

    public void deleteImages(int albumId) {
        albumService.deleteImages(albumId);
    }

    private List<RecordDto> convertToRecordAndSongDto(List<Record> records) {
        List<RecordDto> recordList = new ArrayList<>();

        for (Record record : records) {
            List<TrackDto> trackList = new ArrayList<>();
            record.getTracks().stream()
                    .forEach(track -> trackList.add(convertTrackToDto(track)));
            RecordDto recordDto = convertRecordToDto(record);
            recordDto.setTrackList(trackList);
            recordList.add(recordDto);
        }
        return recordList;
    }

    private RecordDto convertRecordToDto(Record record) {
        RecordDto dto = new RecordDto();
        dto.setNumber(record.getNumber());
        return dto;
    }

    private TrackDto convertTrackToDto(Track track) {
        TrackDto dto = new TrackDto();
        dto.setId(track.getId());
        dto.setTime(track.getTime());
        dto.setTitle(track.getTitle());
        dto.setSide(track.getSide());
        return dto;
    }

    private void sortedImagesNames(Album album) {
        album.setImages(album.getImages()
                .stream().sorted((o1, o2) -> o1.getImageName().compareTo(o2.getImageName()))
                .collect(Collectors.toList()));
    }

    private List<AlbumDto> creatListOfAlbums(List<Album> albums) {
        List<AlbumDto> albumList = new ArrayList<>();
        albums.stream()
                .forEach(album -> albumList.add(convertToAlbumDto(album)));
        return albumList;
    }

    private AlbumForm createBasicForm(AlbumForm form) {
        form.setGroups(groupService.getAllGroups());
        form.setArtists(artistService.getAllArtists());
        form.setMediaTypes(Arrays.asList(MediaType.values()));
        form.setStyles(Arrays.asList(Style.values()));
        return form;
    }

    private AlbumForm createAlbumForm(List<Album> albums) {
        AlbumForm form = new AlbumForm();
//TODO        voor detailscherm
//        form.setAlbum((albums.size() == 1) ? convertToAlbumDto(albums.get(0)) : null);
//        form.setAlbumList((albums.size() > 1) ? creatListOfAlbums(albums) : null);
        albums.stream().forEach(album -> sortedImagesNames(album));
        form.setAlbumList(creatListOfAlbums(albums));
        return form;
    }

    private Album convertToAlbum(Album album, AlbumDto dto) {
        album.setTitle(dto.getTitle());
        album.setReleaseYear(dto.getReleaseYear());
        album.setCollection(dto.isCollection());
        album.setMediaType(MediaType.valueOf(dto.getMedia()));
        album.setStyle(Style.valueOf(dto.getStyle()));
        album.setLabel(dto.getLabel());
        album.setRecordCount(setAmountOfrecords(dto.getMedia()) == 0 ? dto.getTotalRecords()
                : setAmountOfrecords(dto.getMedia()));

        if (dto.getMultipartFiles().length != 0) {
            Arrays.stream(dto.getMultipartFiles())
                    .sequential()
                    .forEach(o -> album.getImages().add(new Image(o.getOriginalFilename(), album)));
        }

        if (!dto.getArtist().equals("NONE")) {
            album.setArtist(artistService.getArtistById(Integer.parseInt(dto.getArtist())));
        }

        if (!dto.getGroup().equals("NONE")) {
            album.setGroup(groupService.getGroupById(Integer.parseInt(dto.getGroup())));
        }

        return album;
    }

    private AlbumDto convertToAlbumDto(Album album) {
        AlbumDto dto = new AlbumDto();
        dto.setId(album.getId());
        dto.setTitle(album.getTitle());
        dto.setReleaseYear(album.getReleaseYear());
        album.getImages().stream().forEach(image -> dto.getImageIds().add(image.getId()));
        dto.setCollection(album.isCollection());
        dto.setArtist(getArtist(album));
        dto.setArtistId(getArtistId(album));
        dto.setGroup(getGroup(album));
        dto.setGroupId(getGroupId(album));
        dto.setMedia(album.getMediaType().getMediaType());
        dto.setMediaType((album.getMediaType()));
        dto.setStyle(album.getStyle().getStyle());
        dto.setStyleType(album.getStyle());
        dto.setLabel(album.getLabel());
        dto.setTotalRecords(album.getRecordCount());
        return dto;
    }

    private String getGroup(Album album) {
        return album.getGroup() != null
                ? album.getGroup().getName()
                : null;
    }

    private String getGroupId(Album album) {
        return String.valueOf(album.getGroup() != null
                ? album.getGroup().getId()
                : null);
    }

    private String getArtist(Album album) {
        return album.getArtist() != null
                ? album.getArtist().getFirstname() + " " + album.getArtist().getLastname()
                : null;
    }

    private String getArtistId(Album album) {
        return String.valueOf(album.getArtist() != null
        ? album.getArtist().getId()
                : null);
    }

    private Integer setAmountOfrecords(String mediaType) {
        if (mediaType.equals(MediaType.MT1.name())
                || mediaType.equals(MediaType.MT3.name())
                || mediaType.equals(MediaType.MT5.name())
                || mediaType.equals(MediaType.MT6.name())
                || mediaType.equals(MediaType.MT7.name())
                || mediaType.equals(MediaType.MT8.name())) {
            return 1;
        }

        if (mediaType.equals(MediaType.MT2.name())
                || mediaType.equals(MediaType.MT4.name())
                || mediaType.equals(MediaType.MT9.name())) {
            return 2;
        }
        return 0;
    }
}


