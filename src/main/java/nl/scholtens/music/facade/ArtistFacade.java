package nl.scholtens.music.facade;

import nl.scholtens.music.From.ArtistForm;
import nl.scholtens.music.domain.Artist;
import nl.scholtens.music.domain.Image;
import nl.scholtens.music.dto.ArtistDto;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.service.ArtistService;
import nl.scholtens.music.service.FilesService;
import nl.scholtens.music.service.SetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ArtistFacade {

    @Autowired
    private ArtistService artistService;

    @Autowired
    private SetupService setupService;

    @Autowired
    private FilesService filesService;

    public ArtistForm getAllAtrists() {
        ArtistForm form = new ArtistForm();
        form.setArtistsList(createListOfArtist(artistService.getAllArtists()));
        return form;
    }

    public ArtistForm getAllAtristsSorted(String sortItem, String sortType) {
        ArtistForm form = new ArtistForm();
        form.setArtistsList(createListOfArtist(artistService.getAllArtistsSorted(sortItem, sortType)));
        return form;
    }

    public ArtistForm getAllAtristsSorted(String sortItem, String sortType, String searchFor) {
        ArtistForm form = new ArtistForm();
        List<Artist> list = artistService.findArtistByName(searchFor);

        switch (sortItem) {
            case "firstname":
                form.setArtistsList(sortType.equals("asc") ? createListOfArtist(list.stream()
                        .sorted(Comparator.comparing(Artist::getFirstname))
                        .collect(Collectors.toList()))
                        : createListOfArtist(list.stream()
                        .sorted(Comparator.comparing(Artist::getFirstname).reversed())
                        .collect(Collectors.toList())));
                return form;
            case "lastname":
                form.setArtistsList((sortType.equals("asc") ? createListOfArtist(list.stream()
                        .sorted(Comparator.comparing(Artist::getLastname))
                        .collect(Collectors.toList()))
                        : createListOfArtist(list.stream()
                        .sorted(Comparator.comparing(Artist::getLastname).reversed())
                        .collect(Collectors.toList()))));
                return form;
        }
        return form;
    }

    public ArtistForm getArtistById(Integer id, boolean updateArtist) {
        ArtistForm form = new ArtistForm();
        form.setUpdateArtist(updateArtist);
        form.setArtist(converToAristDTO(artistService.getArtistById(id)));
        return form;
    }

    public ArtistForm findArtistByName(String name) {
        ArtistForm form = new ArtistForm();
        List<Artist> artists = artistService.findArtistByName(name);
        form.setArtistsList(createListOfArtist(artists));
        return form;
    }

    public void saveArtist(ArtistDto dto) {
        Artist artist = artistService.getArtistById(dto.getId());
        if (artist == null) {
            artistService.saveArtist(converToArtist(dto, new Artist(), false));
        } else {
            artistService.saveArtist(converToArtist(dto, artist, true));
        }
        filesService.uploadFile(dto.getMultipartFile(), Constant.IMAGE_PATH);
    }

    public void deleteArtist(Integer id) {
        artistService.deleteArtist(artistService.getArtistById(id));
    }

    public void deleteImages(int artistId) {
        artistService.delImage(artistId);
    }

    private List<ArtistDto> createListOfArtist(List<Artist> artists) {
        List<ArtistDto> list = new ArrayList<>();
        artists.stream()
                .forEach(artist -> list.add(converToAristDTO(artist)));
        return list;
    }

    private Artist converToArtist(ArtistDto dto, Artist artist, boolean exist) {
        artist.setId(dto.getId());
        artist.setFirstname(dto.getFirstname());
        artist.setLastname(dto.getLastname());

        if (exist && artist.getImage() != null) {
            artist.getImage().setImageName(dto.getMultipartFile().getOriginalFilename());
        } else {
            artist.setImage(new Image(dto.getMultipartFile().getOriginalFilename(), artist));
        }
        return artist;
    }

    private ArtistDto converToAristDTO(Artist artist) {
        ArtistDto dto = new ArtistDto();
        dto.setId(artist.getId());
        dto.setFirstname(artist.getFirstname());
        dto.setLastname(artist.getLastname());
        if (artist.getImage() != null) {
            dto.setImageId(artist.getImage().getId());
        }
        return dto;
    }
}



