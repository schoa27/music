package nl.scholtens.music.facade;

import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.service.FilesService;
import nl.scholtens.music.service.SetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public class SetupFacade {

    @Autowired
    private SetupService setupService;

    @Autowired
    private FilesService filesService;

    public void writeSetupFile(String paths[]) {
        filesService.createMaps(paths);
        setupService.writeSetupFile(paths);
    }

    public String readFilePath() {
        String[] paths = readIniFile();
        return paths.length > 0 ? paths[0]
                                : new String();
    }

    public void setConstant() {
        String[] iniFile = readIniFile();
        if (iniFile.length == 2) {
            Constant.IMAGE_PATH = iniFile[0];
            Constant.XML_PATH = iniFile[1];
        }
    }

    private String[] readIniFile() {
        return setupService.readSetupFile();
    }
}
