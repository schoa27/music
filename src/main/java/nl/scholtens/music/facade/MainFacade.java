package nl.scholtens.music.facade;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.domain.Image;
import nl.scholtens.music.service.AlbumService;
import nl.scholtens.music.service.MainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class MainFacade {

    @Autowired
    private MainService mainService;


    public List<Integer> getImageIdsOfAlbum() {
        Album album = getAnAlblum();

        List<Image> images = album.getImages();
        return images.stream().map(Image::getId).collect(Collectors.toList());

    }

    private List<Integer> getAllAlbumIds() {
        return mainService.allIdOfAllAlbums();
    }

    private Album getAnAlblum() {
        Album album = null;

        if (getAllAlbumIds().size() > 0) {
            while (album == null) {
                int random = new Random().nextInt(getAllAlbumIds().size()) + 1;
                Integer i = mainService.allIdOfAllAlbums().get(random - 1);
                album = mainService.getAlBumById(i);
                album.setImages(album.getImages()
                        .stream()
                        .sorted(((o1, o2) -> o1.getImageName().compareTo(o2.getImageName())))
                        .collect(Collectors.toList()));
                return album;
            }
        }
        return new Album();
    }
}
