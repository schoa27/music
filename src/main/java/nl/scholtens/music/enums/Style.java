package nl.scholtens.music.enums;

public enum Style {
    S1("Pop"),
    S2("Rock"),
    S4("Disco"),
    S5("Soul"),
    S6("Symfonische Rock"),
    S7("New Age"),
    S8("Jazz"),
    S9("Jazz Rock"),
    S10("Klassiek"),
    S11("Progressive Rock"),
    S12("Ambient"),
    S13("Psychedelische Rock"),
    S14("KrautRock"),
    S15("Blues"),
    S16("Popjazz"),
    S17("Reggae"),
    S18("Elektronisch/Synthesizer"),
    S19("SingeSong"),
    S20("Berlin school"),
    NONE("");


    private String style;

    Style(String style) {
        this.style = style;
    }

    public String getStyle() {
        return style;
    }
}
