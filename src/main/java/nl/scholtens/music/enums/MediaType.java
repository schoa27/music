package nl.scholtens.music.enums;


public enum MediaType {

    MT1("Elpee"),
    MT2("Dubbel Eplee"),
    MT3("CD"),
    MT4("Dubbel CD"),
    MT5("Single"),
    MT6("Single CD"),
    MT7("Maxi Single/EP"),
    MT8("DVD"),
    MT9("Dubbel DVD"),
    MT10("Elpee Box"),
    MT11("CD Box"),
    MT12("CD Single Box"),
    MT13("Single Box"),
    MT14("DVD Box"),
    MT15("Picture Disk(s)"),
    MT16("Elpee/CD Box"),
    NONE("");


    private String mediaType;

    private MediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaType() {
        return mediaType;
    }
}
