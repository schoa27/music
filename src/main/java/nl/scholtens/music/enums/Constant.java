package nl.scholtens.music.enums;

public class Constant {

    public final static String ALL = "ALL";

    public final static String NONE = "NONE";

    public static String[] getMediaTypes() {
        return new String[]{"MT3", "MT4", "MT6", "MT11", "MT12", "M13"};
    }

    public static String XML_PATH = "";

    public static String IMAGE_PATH = "";
}
