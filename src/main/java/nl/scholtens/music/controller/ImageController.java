package nl.scholtens.music.controller;

import nl.scholtens.music.From.AlbumForm;
import nl.scholtens.music.domain.Image;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.repository.ImageRepository;
import nl.scholtens.music.service.SetupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Controller
public class ImageController {

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private SetupService setupService;

    @RequestMapping(value = "/image/{imageId}", method = RequestMethod.GET)
    public @ResponseBody
    byte[] getImage(@PathVariable(value = "imageId") int imageId) throws IOException {
//        String[] musicIni = setupService.readSetupFile();
        Image image = imageRepository.findById(imageId).get();
        File file = new File(Constant.IMAGE_PATH + image.getImageName() );
        return Files.readAllBytes(file.toPath());


    }
}
