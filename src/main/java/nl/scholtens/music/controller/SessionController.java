package nl.scholtens.music.controller;

import nl.scholtens.music.domain.MusicSession;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public abstract class SessionController {


    protected void setMusicSession(HttpServletRequest request, String searchItem, String searchFor) {
        MusicSession musicSession = (MusicSession) request.getSession().getAttribute("musicSession");
        musicSession.setSearchFor(searchFor);
        musicSession.setSearchItem(searchItem);
        request.getSession().setAttribute("musicSession", musicSession);
    }

    protected MusicSession getMusicSession(String sortItem, HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("musicSession",setSelection(session, sortItem));
        return (MusicSession) session.getAttribute("musicSession");
    }

    private MusicSession setSelection(HttpSession session, String sortItem) {
        MusicSession musicSession = (MusicSession) session.getAttribute("musicSession");
        String orderSort = musicSession.getSortItem().get(sortItem);

        if (orderSort.equals("asc")) {
            musicSession.getSortItem().put(sortItem,"desc");
        } else {
            musicSession.getSortItem().put(sortItem,"asc");
        }
        return musicSession;
    }
}
