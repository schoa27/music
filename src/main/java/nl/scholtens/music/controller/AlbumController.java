package nl.scholtens.music.controller;


import nl.scholtens.music.From.AlbumForm;
import nl.scholtens.music.domain.MusicSession;
import nl.scholtens.music.facade.AlbumFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;


@Controller
public class AlbumController extends SessionController {

    @Autowired
    private AlbumFacade albumFacade;

    @RequestMapping(value = "/album/allAlbums", method = RequestMethod.GET)
    public ModelAndView getAllAlbumsGet(HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, null, null, albumFacade.getAllAlbums());
        return model;
    }

    @RequestMapping(value = "/album/allAlbums", method = RequestMethod.POST)
    public ModelAndView getAllAlbumsPost(HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, null, null, albumFacade.getAllAlbums());
        return model;
    }

    @RequestMapping(value = "/album/sort/{item}", method = RequestMethod.GET)
    public ModelAndView getAllAlbumsSorted(@PathVariable(name = "item") String sortItem, HttpServletRequest request) {
        ModelAndView model = new ModelAndView("album_list");
        MusicSession musicSession = getMusicSession(sortItem, request);
        model.addObject("form", musicSession.getSearchFor() != null
                ? albumFacade.getAllAlbumsSorted(sortItem, musicSession.getSortItem(), musicSession.getSearchItem(), musicSession.getSearchFor())
                : albumFacade.getAllAlbumsSorted(sortItem, musicSession.getSortItem().get(sortItem)));
        return model;
    }

    @RequestMapping(value = "/album/details/{id}", method = RequestMethod.GET)
    public ModelAndView getDetailAlbums(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("album_details");
        AlbumForm form = albumFacade.getAlbumById(id, false);
        model.addObject("form", form);
        return model;
    }

    @RequestMapping(value = "/album/images/{id}", method = RequestMethod.GET)
    public ModelAndView getAlbumImages(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("album_images");
        AlbumForm form = albumFacade.getAlbumById(id, false);
        model.addObject("form", form);
        return model;
    }

    @RequestMapping(value = "/album/albumByTitle/{title}", method = RequestMethod.GET)
    public ModelAndView findAlbumByTitle(@PathVariable(value = "title") String title, HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, "title", title, albumFacade.getAlbumByTitle(title));
        return model;
    }

    @RequestMapping(value = "/album/albumByArtist/{artist}", method = RequestMethod.GET)
    public ModelAndView findAlbumByArtist(@PathVariable(value = "artist") String artist, HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, "artist", artist, albumFacade.getAlbumByArtist(artist));
        return model;
    }

    @RequestMapping(value = "/album/albumByGroup/{group}", method = RequestMethod.GET)
    public ModelAndView findAlbumByGroup(@PathVariable(value = "group") String group, HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, "group", group, albumFacade.getAlbumByGroup(group));
        return model;
    }

    @RequestMapping(value = "/album/albumByYear/{year}", method = RequestMethod.GET)
    public ModelAndView findAlbumByReleaseYear(@PathVariable(value = "year") String year, HttpServletRequest request ) {
        ModelAndView model = getModelAndView(request, "releaseYear", year, albumFacade.getAlbumByReleaseYear(year));
        return model;
    }

    @RequestMapping(value = "/album/albumByMedia/{media}", method = RequestMethod.GET)
    public ModelAndView findAlbumByMedia(@PathVariable(value = "media") String media, HttpServletRequest request ) {
        ModelAndView model = getModelAndView(request, "media", media, albumFacade.getAlbumByMediaType(media));
        return model;
    }

    @RequestMapping(value = "/album/albumByStyle/{style}", method = RequestMethod.GET)
    public ModelAndView findAlbumByStyle(@PathVariable(value = "style") String style, HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, "style", style, albumFacade.getAlbumByStyle(style));
        return model;
    }

    @RequestMapping(value = "/album/addAlbum", method = RequestMethod.GET)
    public ModelAndView addAlbum() {
        return getModelAndView();
    }

    @RequestMapping(value = "/album/addAlbum", method = RequestMethod.POST)
    public ModelAndView addAlbumPost() {
        return getModelAndView();
    }

    @RequestMapping(value = "/album/saveAlbum", method = RequestMethod.POST)
    public ModelAndView saveAlbum(@ModelAttribute("albumForm") AlbumForm form) {
        albumFacade.saveAlbum(form.getAlbum());
        return new ModelAndView("redirect:/album/allAlbums");
    }

    @RequestMapping(value = "/album/updateAlbum/{id}", method = RequestMethod.GET)
    public ModelAndView updateArtist(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("album_form");
        model.addObject("form", albumFacade.getAlbumById(id, false));
        return model;
    }

    @RequestMapping(value = "/album/deleteAlbum/{id}", method = RequestMethod.GET)
    public ModelAndView deleteArtist(@PathVariable(value = "id") Integer id, ModelAndView model) {
        albumFacade.deleteAlbum(id);
        return new ModelAndView("redirect:/album/allAlbums");
    }

    @RequestMapping(value = "/album/image/delete/{albumId}", method = RequestMethod.POST)
    public ModelAndView deleteImage(@PathVariable(value = "albumId") int albumId) {
        ModelAndView model = new ModelAndView("album_form");
        albumFacade.deleteImages(albumId);
        AlbumForm form = albumFacade.getAlbumById(albumId, false);
        form.setDelImage(true);
        model.addObject("form", form);
        return model;
    }

    @RequestMapping(value = "/album/backwards/{id}", method = RequestMethod.GET)
    public ModelAndView backwards(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("album_details");
        AlbumForm form = albumFacade.getAlbumById(albumFacade.bepaalNewAlbumId(id, false), false);
        model.addObject("form", form);
        return model;
    }

    @RequestMapping(value = "/album/forwards/{id}", method = RequestMethod.GET)
    public ModelAndView forwards(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("album_details");
        AlbumForm form = albumFacade.getAlbumById(albumFacade.bepaalNewAlbumId(id, true), false);
        model.addObject("form", form);
        return model;
    }

    private ModelAndView getModelAndView(HttpServletRequest request, String objectName, String object, AlbumForm allAlbums) {
        ModelAndView model = new ModelAndView("album_list");
        setMusicSession(request, objectName, object);
        model.addObject("form", allAlbums);
        return model;
    }

    private ModelAndView getModelAndView() {
        ModelAndView model = new ModelAndView("album_form");

        AlbumForm form = albumFacade.getAlbumById(null, true);
        form.setDelImage(true);

        model.addObject("form", form);
        return model;
    }
}
