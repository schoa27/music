package nl.scholtens.music.controller;

import nl.scholtens.music.dto.MusicSearchDto;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class SearchController {

    @RequestMapping(value = "/artist/search", method = RequestMethod.GET)
    public ModelAndView searchArtists() {
        ModelAndView model = new ModelAndView("search_form");
        model.addObject("searchForm", new MusicSearchDto("artist"));
        return model;
    }

    @RequestMapping(value = "/artist/searchitem", method = RequestMethod.POST)
    public ModelAndView searchItemArtist(@Valid @ModelAttribute("searchForm") MusicSearchDto search,
                                         BindingResult bindingResult, ModelAndView model) {
        if (CheckError(search, bindingResult, model, "artist")) return model;
        return new ModelAndView("redirect:/artist/findByName/" + search.getSearchFor());
    }

    @RequestMapping(value = "/group/search", method = RequestMethod.GET)
    public ModelAndView searchGroups() {
        ModelAndView model = new ModelAndView("search_form");
        model.addObject("searchForm", new MusicSearchDto("group"));
        return model;
    }

    @RequestMapping(value = "/group/searchitem", method = RequestMethod.POST)
    public ModelAndView searchItemGroup(@Valid @ModelAttribute("searchForm") MusicSearchDto search,
                                        BindingResult bindingResult, ModelAndView model) {
        if (CheckError(search, bindingResult, model, "group")) return model;
        return new ModelAndView("redirect:/group/findByName/" + search.getSearchFor());
    }

    @RequestMapping(value = "/album/search", method = RequestMethod.GET)
    public ModelAndView searchAlbumss() {
        ModelAndView model = new ModelAndView("search_form");
        model.addObject("searchForm", new MusicSearchDto("album"));
        return model;
    }

    @RequestMapping(value = "/album/searchitem", method = RequestMethod.POST)
    public ModelAndView searchItemAlbum(@Valid @ModelAttribute("searchForm") MusicSearchDto search,
                                        BindingResult bindingResult, ModelAndView model) {

        switch (search.getChoose()) {
            case "title":
                if (CheckError(search, bindingResult, model, "album")) return model;
                return new ModelAndView("redirect:/album/albumByTitle/" + search.getSearchFor());
            case "artist":
                if (CheckError(search, bindingResult, model, "album")) return model;
                return new ModelAndView("redirect:/album/albumByArtist/" + search.getSearchFor());
            case "group":
                if (CheckError(search, bindingResult, model, "album")) return model;
                return new ModelAndView("redirect:/album/albumByGroup/" + search.getSearchFor());
            case "year":
                if (CheckError(search, bindingResult, model, "album")) return model;
                return new ModelAndView("redirect:/album/albumByYear/" + search.getSearchFor());
            case "media":
                return new ModelAndView("redirect:/album/albumByMedia/" + search.getMedia());
            case "style":
                return new ModelAndView("redirect:/album/albumByStyle/" + search.getStyle());
        }
        return new ModelAndView("redirect:/album/allAlbums");
    }

    private boolean CheckError(@ModelAttribute("searchForm") @Valid MusicSearchDto search, BindingResult bindingResult, ModelAndView model, String item) {
        if (bindingResult.hasErrors()) {
            search.setSearchItem(item);
            model.addObject("searchForm", search);
            model.setViewName("search_form");
            return true;
        }
        return false;
    }
}


