package nl.scholtens.music.controller;


import nl.scholtens.music.From.ArtistForm;
import nl.scholtens.music.From.GroupForm;
import nl.scholtens.music.domain.MusicSession;
import nl.scholtens.music.facade.GroupFacade;
import nl.scholtens.music.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
public class GroupController extends SessionController {

    @Autowired
    private GroupFacade groupFacade;


    @RequestMapping(value = "/group/allGroups", method = RequestMethod.GET)
    public ModelAndView getAllAlbumsGet(HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, null, null, groupFacade.getAllGroups());
        return model;
    }

    @RequestMapping(value = "/group/allGroups", method = RequestMethod.POST)
    public ModelAndView getAllAlbumsPost(HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, null, null, groupFacade.getAllGroups());
        return model;
    }

    @RequestMapping(value = "/group/sort/{item}", method = RequestMethod.GET)
    public ModelAndView getAllArtistSorted(@PathVariable(name = "item") String sortItem, HttpServletRequest request) {
        ModelAndView model = new ModelAndView("group_list");
        MusicSession musicSession = getMusicSession(sortItem, request);
        model.addObject("groupForm", musicSession.getSearchFor() != null
                ? groupFacade.getAllGroupsByNameSorted(sortItem, musicSession.getSortItem().get(sortItem), musicSession.getSearchFor())
                : groupFacade.getAllGroupsSorted(sortItem, musicSession.getSortItem().get(sortItem)));
        return model;
    }

    @RequestMapping(value = "/group/findByName/{group}", method = RequestMethod.GET)
    public ModelAndView findArtist(@PathVariable(value = "group") String group, HttpServletRequest request) {
        ModelAndView model = getModelAndView(request, "group", group, groupFacade.findGroupByName(group));
        return model;
    }

    @RequestMapping(value = "/group/addGroup", method = RequestMethod.GET)
    public ModelAndView addGroup() {
        ModelAndView model = new ModelAndView("group_form");
        GroupForm form = new GroupForm();
        form.setDelImage(true);

        model.addObject("groupForm", form);
        return model;
    }

    @RequestMapping(value = "/group/save", method = RequestMethod.POST)
    public ModelAndView saveGroup(@ModelAttribute("groupForm") GroupForm form) {
        groupFacade.saveGroup(form.getGroup());
        return new ModelAndView("redirect:/group/allGroups");
    }

    @RequestMapping(value = "/group/updateGroup/{id}", method = RequestMethod.GET)
    public ModelAndView updateArtist(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("group_form");
        model.addObject("groupForm", groupFacade.getGroupById(id, true));
        return model;
    }

    @RequestMapping(value = "/group/deleteGroup/{id}", method = RequestMethod.GET)
    public ModelAndView deleteGroup(@PathVariable(value = "id") Integer id) {
        groupFacade.deleteGroup(id);
        return new ModelAndView("redirect:/group/allGroups");
    }

    @RequestMapping(value = "/group/image/delete/{groupId}", method = RequestMethod.POST)
    public ModelAndView deleteImage(@PathVariable(value = "groupId") int groupId) {
        ModelAndView model = new ModelAndView("group_form");
        groupFacade.deleteImages(groupId);
        GroupForm form = groupFacade.getGroupById(groupId, false);
        form.setDelImage(true);
        model.addObject("groupForm", form);
        return model;
    }

    private ModelAndView getModelAndView(HttpServletRequest request, String objectName, String object, GroupForm allGroups) {
        ModelAndView model = new ModelAndView(("group_list"));
        setMusicSession(request, objectName, object);
        model.addObject("groupForm", allGroups);
        return model;
    }
}


