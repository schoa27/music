package nl.scholtens.music.controller;

import nl.scholtens.music.From.AlbumForm;
import nl.scholtens.music.From.ArtistForm;
import nl.scholtens.music.domain.MusicSession;
import nl.scholtens.music.facade.ArtistFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;


@Controller
public class ArtistController extends SessionController {

    @Autowired
    private ArtistFacade artistFacade;


    @RequestMapping(value = "/artist/allArtists", method = RequestMethod.GET)
    public ModelAndView getAllAlbumsGet(HttpServletRequest request) {
        ModelAndView model = getModelAndView(request);
        return model;
    }

    @RequestMapping(value = "/artist/allArtists", method = RequestMethod.POST)
    public ModelAndView getAllAlbumsPost(HttpServletRequest request) {
        ModelAndView model = getModelAndView(request);
        return model;
    }

    @RequestMapping(value = "/artist/sort/{item}", method = RequestMethod.GET)
    public ModelAndView getAllArtistSorted(@PathVariable(name = "item") String sortItem, HttpServletRequest request) {
        ModelAndView model = new ModelAndView("artist_list");
        MusicSession musicSession = getMusicSession(sortItem, request);
        musicSession.setSearchItem(sortItem);
        model.addObject("artistForm", musicSession.getSearchFor() != null
                ? artistFacade.getAllAtristsSorted(sortItem, musicSession.getSortItem().get(sortItem), musicSession.getSearchFor())
                : artistFacade.getAllAtristsSorted(sortItem, musicSession.getSortItem().get(sortItem)));
        return model;
    }

    @RequestMapping(value = "/artist/findByName/{name}", method = RequestMethod.GET)
    public ModelAndView findArtist(@PathVariable(value = "name") String name, ModelAndView model, HttpServletRequest request) {
        model.addObject("artistForm", artistFacade.findArtistByName(name));
        setMusicSession(request, null, name);
        model.setViewName("artist_list");
        return model;
    }

    @RequestMapping(value = "/artist/addArtist", method = RequestMethod.GET)
    public ModelAndView addArtist() {
        ModelAndView model = new ModelAndView("artist_form");
        ArtistForm form = new ArtistForm();
        form.setDelImage(true);

        model.addObject("artistForm", form);
        return model;
    }

    @RequestMapping(value = "/artist/save", method = RequestMethod.POST)
    public ModelAndView saveArtist(@ModelAttribute("artistForm") ArtistForm form) {
        artistFacade.saveArtist(form.getArtist());
        return new ModelAndView("redirect:/artist/allArtists");
    }

    @RequestMapping(value = "/artist/updateArtist/{id}", method = RequestMethod.GET)
    public ModelAndView updateArtist(@PathVariable(value = "id") Integer id) {
        ModelAndView model = new ModelAndView("artist_form");
        model.addObject("artistForm", artistFacade.getArtistById(id, true));
        return model;
    }

    @RequestMapping(value = "/artist/deleteArtist/{id}", method = RequestMethod.GET)
    public ModelAndView deleteArtist(@PathVariable(value = "id") Integer id, ModelAndView model) {
        artistFacade.deleteArtist(id);
        return new ModelAndView("redirect:/artist/allArtists");
    }

    @RequestMapping(value = "/artist/image/delete/{artistId}", method = RequestMethod.POST)
    public ModelAndView deleteImage(@PathVariable(value = "artistId") int artistId) {
        ModelAndView model = new ModelAndView("artist_form");
        artistFacade.deleteImages(artistId);
        ArtistForm form = artistFacade.getArtistById(artistId, false);
        form.setDelImage(true);
        model.addObject("artistForm", form);
        return model;
    }

    private ModelAndView getModelAndView(HttpServletRequest request) {
        ModelAndView model = new ModelAndView("artist_list");
        setMusicSession(request, null, null);
        model.addObject("artistForm", artistFacade.getAllAtrists());
        return model;
    }
}


