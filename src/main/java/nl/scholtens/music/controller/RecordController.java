package nl.scholtens.music.controller;


import nl.scholtens.music.From.RecordForm;
import nl.scholtens.music.dto.RecordDto;
import nl.scholtens.music.facade.RecordFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RecordController extends SessionController {


    @Autowired
    private RecordFacade recordFacade;

    @RequestMapping(value = "/record/addRecord/{id}/{records}/{media}/{collection}", method = RequestMethod.GET)
    public ModelAndView getAllAlbums(@PathVariable(name = "id") Integer id,
                                     @PathVariable(name = "records") Integer records,
                                     @PathVariable(name = "media") String media,
                                     @PathVariable(name = "collection") boolean collection) {
        ModelAndView model = new ModelAndView("record_form");
        RecordForm form = recordFacade.createRecordForm(records, id);
        model.addObject("form", form);
        return model;
    }

    @RequestMapping(value = "/record/saveTrack")
    public ModelAndView saveRecordAndSongs(@ModelAttribute RecordForm form) {
        ModelAndView model = new ModelAndView();
        Integer albumId = form.getAlbumId();
        RecordDto record = form.getRecord();
        Integer trackId = form.getTrackId();

        recordFacade.saveRecord(albumId, record, trackId);
        form = recordFacade.createRecordForm(form.getRecordCount(), albumId);
        model.addObject("form", form);
        model.setViewName("record_form");
        return model;
    }

    @RequestMapping(value = "/track/updateTrack/{albumId}/{trackId}")
    public ModelAndView updateTrack(@PathVariable(name = "albumId") Integer albumId,
                                    @PathVariable(name = "trackId") Integer trackId) {
        RecordForm recordForm = recordFacade.getRecordById(albumId, trackId);
        ModelAndView model = new ModelAndView();
        model.addObject("form", recordForm);
        model.setViewName("record_form");
        return model;
    }

    @RequestMapping(value = "/track/deleteTrack/{albumId}/{trackId}")
    public ModelAndView deleteTrack(@PathVariable(name = "albumId") Integer albumId,
                                    @PathVariable(name = "trackId") Integer trackId) {
        recordFacade.deleteTrack(trackId);

        ModelAndView model = new ModelAndView();
        RecordForm recordForm = recordFacade.createRecordForm(recordFacade.getNumberOfRecordsByAlbumId(albumId), albumId);
        model.addObject("form", recordForm);
        model.setViewName("record_form");
        return model;
    }

}
