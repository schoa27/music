package nl.scholtens.music.controller;

import nl.scholtens.music.From.BeheerForm;
import nl.scholtens.music.dto.MusicSearchDto;
import nl.scholtens.music.facade.BeheerFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BeheerController {

    @Autowired
    private BeheerFacade facade;

    @RequestMapping(value = "/beheer/export", method = RequestMethod.GET)
    public ModelAndView exportBeheer() {
        ModelAndView mav = new ModelAndView("beheer_form");
        mav.addObject("form", facade.createExportForm());
        return mav;
    }

    @RequestMapping(value = "/beheer/albums", method = RequestMethod.POST)
    public ModelAndView exportBeheerAlbums(@ModelAttribute("form") BeheerForm form,
                                           BindingResult bindingResult) {
        ModelAndView model = new ModelAndView("redirect:/music");

        if (form.isExportBeheer())  {
            form.setTotal(facade.exportCriteria(form));
        }

        if (form.isImportBeheer()) {
            form.setTotal(facade.importCriteria(form));
        }

        form.setResult(true);
        form.setFieldset("disabled");
        model.addObject("form", form);
        model.setViewName("beheer_form");
        return model;
    }

    @RequestMapping(value = "/beheer/import", method = RequestMethod.GET)
    public ModelAndView importBeheer() {
        ModelAndView mav = new ModelAndView("beheer_form");
        mav.addObject("form", facade.createImportForm());
        return mav;
    }
}
