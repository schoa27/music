package nl.scholtens.music.controller;

import nl.scholtens.music.domain.MusicSession;
import nl.scholtens.music.domain.MusicSetup;
import nl.scholtens.music.facade.MainFacade;
import nl.scholtens.music.facade.SetupFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MainController {

    @Autowired
    private SetupFacade setupFacade;

    @Autowired
    private MainFacade mainFacade;

    @Autowired
    BuildProperties buildProperties;

    @RequestMapping(value = "/music", method = RequestMethod.GET)
    public ModelAndView mainMenuGet(HttpServletRequest request) {
        request.getSession().setAttribute("musicSession", new MusicSession(buildProperties.getVersion()));
        String path = setupFacade.readFilePath();
        setupFacade.setConstant();
        return path.isEmpty() ? new ModelAndView("redirect:/setup")
                              : getAnImage();
    }

    @RequestMapping(value = "/music", method = RequestMethod.POST)
    public ModelAndView mainMenuPost(HttpServletRequest request) {
        request.getSession().setAttribute("musicSession", new MusicSession(buildProperties.getVersion()));
        return getAnImage();
    }

    @RequestMapping(value = "/setup", method = RequestMethod.GET)
    public ModelAndView setup() {
        ModelAndView model = new ModelAndView("menu_setup");
        model.addObject("setupForm", new MusicSetup());
        return model;
    }

    @RequestMapping(value = "/setup/save", method = RequestMethod.GET)
    public ModelAndView setupSave(@ModelAttribute("setupForm") MusicSetup setup) {
        setupFacade.writeSetupFile(setup.getPath());
        setupFacade.setConstant();
        return new ModelAndView("main_page");
    }

    private ModelAndView getAnImage() {
        ModelAndView model = new ModelAndView("main_page");
        model.addObject("imagepath", setupFacade.readFilePath());
        model.addObject("imageId", mainFacade.getImageIdsOfAlbum());
        return model;
    }

}
