package nl.scholtens.music.repository;

import nl.scholtens.music.domain.Record;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface RecordRepository extends CrudRepository<Record, Integer> {

    Record findRecordById(Integer id);

    List<Record> findRecordByAlbum_IdOrderByNumber(Integer id);

    Record findRecordByAlbum_IdAndNumber(Integer albumId, Integer recordNumber);

    @Query("SELECT COUNT(record) FROM Record AS record WHERE record.album.id = :albumId")
    Integer recordCountByAlbumId(@Param("albumId") int albumId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Record AS record WHERE record.id = :recordId")
    void deleteRecordById(@Param("recordId") int recordId);
}
