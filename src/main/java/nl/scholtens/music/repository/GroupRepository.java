package nl.scholtens.music.repository;

import nl.scholtens.music.domain.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, Integer> {

    List<Group> findByNameContainingIgnoreCase(String name);

    List<Group> findGroupByOrderByNameAsc();

    List<Group> findGroupByOrderByNameDesc();

    List<Group> findByNameContainingIgnoreCaseOrderByNameAsc(String searchFor);

    List<Group> findByNameContainingIgnoreCaseOrderByNameDesc(String searchFor);
}
