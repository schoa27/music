package nl.scholtens.music.repository;

import nl.scholtens.music.domain.Image;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface ImageRepository extends CrudRepository<Image, Integer> {

    @Transactional
    @Modifying
    @Query("DELETE FROM Image AS image WHERE image.album.id = :albumId")
    void delAlbumImages(@Param("albumId") Integer albumId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Image AS image WHERE image.artist.id = :artistId")
    void delArtistImage(@Param("artistId") Integer artistId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Image AS image WHERE image.group.id = :groupId")
    void delGroupImage(@Param("groupId") Integer groupId);


}
