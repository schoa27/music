package nl.scholtens.music.repository;

import nl.scholtens.music.domain.Artist;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtistRepository extends CrudRepository<Artist, Integer> {

    List<Artist> findArtistsByFirstnameContainingIgnoreCase(String firstname);

    List<Artist> findArtistsByLastnameContainingIgnoreCase(String lastname);

    List<Artist> findArtistsByOrderByFirstnameAsc();

    List<Artist> findArtistsByOrderByFirstnameDesc();

    List<Artist> findArtistsByOrderByLastnameAsc();

    List<Artist> findArtistsByOrderByLastnameDesc();
}
