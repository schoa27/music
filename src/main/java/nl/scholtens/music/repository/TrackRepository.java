package nl.scholtens.music.repository;

import nl.scholtens.music.domain.Track;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface TrackRepository extends CrudRepository<Track, Integer> {

    @Query("SELECT track FROM Track AS track WHERE track.record.id = :recordId ORDER BY track.side")
    List<Track> getAllTracksByRecordId(@Param("recordId") int recordIs);

    @Query("SELECT track.record.id FROM Track AS track WHERE track.id = :trackId")
    Integer findRecordIdByTrackId(@Param("trackId") int trackId);

    @Transactional
    @Modifying
    @Query("DELETE FROM Track AS track WHERE track.id = :trackId")
    void deleteByTrackId(@Param("trackId") int trackId);

    @Query("SELECT COUNT(track) FROM Track AS track WHERE track.record.id = :recordId")
    Integer countTrackByRecordId(@Param("recordId") int recordId);
}
