package nl.scholtens.music.repository;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlbumRepository extends CrudRepository<Album, Integer> {

    List<Album> findByTitleContainingIgnoreCase(String searchString);

    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCase(String searchString);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCase(String searchString);

    List<Album> findAlbumsByGroupNameContainingIgnoreCase(String searchString);

    List<Album> findAlbumsByReleaseYear(String year);

    List<Album> findAlbumsByMediaType(MediaType type);

    List<Album> findAlbumsByStyle(Style listStyle);

    List<Album> findAlbumsByOrderByTitleAsc();

    List<Album> findAlbumsByOrderByTitleDesc();

    List<Album> findAlbumsByOrderByReleaseYearAsc();

    List<Album> findAlbumsByOrderByReleaseYearDesc();


    List<Album> findAlbumsByTitleContainingIgnoreCaseOrderByTitleAsc(String title);

    List<Album> findAlbumsByTitleContainingIgnoreCaseOrderByTitleDesc(String title);

    List<Album> findAlbumsByTitleContainingIgnoreCaseAndArtistNotNullOrderByArtistAsc(String artist);

    List<Album> findAlbumsByTitleContainingIgnoreCaseAndArtistNotNullOrderByArtistDesc(String artist);

    List<Album> findAlbumsByTitleContainingIgnoreCaseAndGroupNotNullOrderByGroupAsc(String group);

    List<Album> findAlbumsByTitleContainingIgnoreCaseAndGroupNotNullOrderByGroupDesc(String group);

    List<Album> findAlbumsByTitleContainingIgnoreCaseOrderByReleaseYearAsc(String year);

    List<Album> findAlbumsByTitleContainingIgnoreCaseOrderByReleaseYearDesc(String year);



    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByTitleAsc(String title);

    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByTitleDesc(String title);

    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByArtistAsc(String artist);

    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByArtistDesc(String artist);

    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByReleaseYearAsc(String year);

    List<Album> findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByReleaseYearDesc(String year);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCaseOrderByTitleAsc(String title);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCaseOrderByTitleDesc(String title);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCaseOrderByArtistAsc(String artist);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCaseOrderByArtistDesc(String artist);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCaseOrderByReleaseYearAsc(String year);

    List<Album> findAlbumsByArtistLastnameContainingIgnoreCaseOrderByReleaseYearDesc(String year);


    List<Album> findAlbumsByGroupNameContainingIgnoreCaseOrderByTitleAsc(String title);

    List<Album> findAlbumsByGroupNameContainingIgnoreCaseOrderByTitleDesc(String title);

    List<Album> findAlbumsByGroupNameContainingIgnoreCaseOrderByGroupAsc(String group);

    List<Album> findAlbumsByGroupNameContainingIgnoreCaseOrderByGroupDesc(String group);

    List<Album> findAlbumsByGroupNameContainingIgnoreCaseOrderByReleaseYearAsc(String year);

    List<Album> findAlbumsByGroupNameContainingIgnoreCaseOrderByReleaseYearDesc(String year);


    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseOrderByTitleAsc(String title);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseOrderByTitleDesc(String title);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseAndArtistNotNullOrderByArtistAsc(String artist);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseAndArtistNotNullOrderByArtistDesc(String artist);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseAndGroupNotNullOrderByGroupAsc(String group);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseAndGroupNotNullOrderByGroupDesc(String group);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseOrderByReleaseYearAsc(String year);

    List<Album> findAlbumsByReleaseYearContainingIgnoreCaseOrderByReleaseYearDesc(String year);


    List<Album> findAlbumsByMediaTypeOrderByTitleAsc(MediaType mediaType);

    List<Album> findAlbumsByMediaTypeOrderByTitleDesc(MediaType mediaType);


    List<Album> findAlbumsByMediaTypeAndArtistNotNullOrderByArtistAsc(MediaType mediaType);

    List<Album> findAlbumsByMediaTypeAndArtistNotNullOrderByArtistDesc(MediaType mediaType);

    List<Album> findAlbumsByMediaTypeAndGroupNotNullOrderByGroupAsc(MediaType mediaType);

    List<Album> findAlbumsByMediaTypeAndGroupNotNullOrderByGroupDesc(MediaType mediaType);

    List<Album> findAlbumsByMediaTypeOrderByReleaseYearAsc(MediaType mediaType);

    List<Album> findAlbumsByMediaTypeOrderByReleaseYearDesc(MediaType mediaType);


    List<Album> findAlbumsByStyleOrderByTitleAsc(Style style);

    List<Album> findAlbumsByStyleOrderByTitleDesc(Style style);

    List<Album> findAlbumsByStyleAndArtistNotNullOrderByArtistAsc(Style style);

    List<Album> findAlbumsByStyleAndArtistNotNullOrderByArtistDesc(Style style);

    List<Album> findAlbumsByStyleAndGroupNotNullOrderByGroupAsc(Style style);

    List<Album> findAlbumsByStyleAndGroupNotNullOrderByGroupDesc(Style style);

    List<Album> findAlbumsByStyleOrderByReleaseYearAsc(Style style);

    List<Album> findAlbumsByStyleOrderByReleaseYearDesc(Style style);

//    List<Album> findAllAlbumsByArtistFirstnameAndArtistLastname(String firstname, String lastname);

    Album findAlbumById(int id);

    @Query("SELECT album.id FROM Album AS album ORDER BY album.id ASC")
    List<Integer> getAlAlbumIds();

    @Query("SELECT album FROM Album AS album WHERE CONCAT(album.artist.firstname, ' ' , album.artist.lastname) = :fullName" )
    List<Album> getAllAlbumFromArtistByFirstnameAndLastname(@Param("fullName") String name);
}

