package nl.scholtens.music.xml;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

@XmlType(propOrder = {"firstname", "lastname", "image"})
        public class ArtistXML implements Serializable {

    public ArtistXML() {
        super();
    }

    public ArtistXML(String firstname, String lastname, ImageXML image) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.image = image;
    }

    private String firstname;

    private String lastname;

    private ImageXML image;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @XmlElement(name = "image")
    public ImageXML getImage() {
        return image;
    }

    public void setImage(ImageXML image) {
        this.image = image;
    }

}
