package nl.scholtens.music.xml;

public class ImageXML {

    private String[] imageName;

    public ImageXML() {
        super();
    }

    public ImageXML(String[] imageName) {
        this.imageName = imageName;
    }

    public String[] getImageName() {
        return imageName;
    }

    public void setImageName(String[] imageName) {
        this.imageName = imageName;
    }
}
