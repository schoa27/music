package nl.scholtens.music.xml;

import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "album")
@XmlType(propOrder = {"collection", "title", "releaseYear", "label", "mediaType", "style", "recordCount", "artist", "group", "images", "records"})
public class AlbumXML implements java.io.Serializable {

    private boolean collection;

    private String title;

    private String releaseYear;

    private String label;

    private MediaType mediaType;

    private Style style;

    private Integer recordCount;

    private ArtistXML artist;

    private GroupXML group;

    private ImageXML images;;

    private List<RecordXML> records = new ArrayList<>();

    public boolean isCollection() {
        return collection;
    }

    public void setCollection(boolean collection) {
        this.collection = collection;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public MediaType getMediaType() {
        return mediaType;
    }

    public void setMediaType(MediaType mediaType) {
        this.mediaType = mediaType;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Integer getRecordCount() {
        return recordCount;
    }

    public void setRecordCount(Integer recordCount) {
        this.recordCount = recordCount;
    }

    public ArtistXML getArtist() {
        return artist;
    }

    public void setArtist(ArtistXML artist) {
        this.artist = artist;
    }

    public GroupXML getGroup() {
        return group;
    }

    public void setGroup(GroupXML group) {
        this.group = group;
    }

    @XmlElement(name = "images")
    public ImageXML getImages() {
        return images;
    }

    public void setImages(ImageXML images) {
        this.images = images;
    }

    @XmlElement(name = "record")
    public List<RecordXML> getRecords() {
        return records;
    }

    public void setRecords(List<RecordXML> records) {
        this.records = records;
    }
}



