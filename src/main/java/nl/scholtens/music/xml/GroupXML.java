package nl.scholtens.music.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlType(propOrder = {"name", "image"})
public class GroupXML {

    private String name;

    private ImageXML image;

    public GroupXML() {
        super();
    }

    public GroupXML(String name, ImageXML image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @XmlElement(name = "image")
    public ImageXML getImage() {
        return image;
    }

    public void setImage(ImageXML image) {
        this.image = image;
    }
}
