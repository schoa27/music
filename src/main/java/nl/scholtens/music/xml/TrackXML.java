package nl.scholtens.music.xml;

public class TrackXML {

    private String title;

    private String time;

    private String side;

    public TrackXML() {
        super();
    }

    public TrackXML(String title, String time, String side) {
        this.title = title;
        this.time = time;
        this.side = side;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getSide() {
        return side;
    }

    public void setSide(String side) {
        this.side = side;
    }
}
