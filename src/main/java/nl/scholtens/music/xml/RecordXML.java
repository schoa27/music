package nl.scholtens.music.xml;

import javax.xml.bind.annotation.XmlElement;
import java.util.ArrayList;
import java.util.List;

public class RecordXML {

    private Integer number;

    private String name;

    private ImageXML image;

    private List<TrackXML> tracks = new ArrayList<>();

    public RecordXML() {
        super();
    }

    public RecordXML(Integer number, String name, ImageXML image, List<TrackXML> tracks) {
        this.number = number;
        this.name = name;
        this.image = image;
        this.tracks = tracks;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ImageXML getImage() {
        return image;
    }

    public void setImage(ImageXML image) {
        this.image = image;
    }

    @XmlElement(name = "track")
    public List<TrackXML> getTracks() {
        return tracks;
    }

    public void setTracks(List<TrackXML> tracks) {
        this.tracks = tracks;
    }
}
