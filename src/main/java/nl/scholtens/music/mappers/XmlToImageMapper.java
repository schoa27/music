package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.*;


public class XmlToImageMapper {

    private Image image = new Image();

    public XmlToImageMapper(String imageXML, Album album) {
        xmlMapper(imageXML, album) ;
    }

    public XmlToImageMapper(String imageXML, Artist artist) {
        xmlMapper(imageXML, artist) ;
    }

    public XmlToImageMapper(String imageXML, Group group) {
        xmlMapper(imageXML, group) ;
    }

    public XmlToImageMapper(String imageXML, Record record) {
        xmlMapper(imageXML, record) ;
    }

    public Image getImage() {
        return image;
    }

    private void xmlMapper(String imageXML, Album album) {
        image.setImageName(imageXML);
        image.setAlbum(album);
    }

    private void xmlMapper(String imageXML, Artist artist) {
        image.setImageName(imageXML);
        image.setArtist(artist);
    }

    private void xmlMapper(String imageXML, Group group) {
        image.setImageName(imageXML);
        image.setGroup(group);
    }

    private void xmlMapper(String imageXML, Record record) {
        image.setImageName(imageXML);
        image.setRecord(record);
    }
}
