package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.Group;
import nl.scholtens.music.xml.GroupXML;
import nl.scholtens.music.xml.ImageXML;

public class XmlToGroupMapper {

    private Group group = new Group();

    public XmlToGroupMapper(GroupXML groupXML) {
        xmlMapper(groupXML);
    }

    public Group getGroup() {
        return group;
    }

    private void xmlMapper(GroupXML groupXML) {
        group.setName(groupXML.getName());

        ImageXML imageArray = groupXML.getImage();
        if (imageArray != null && imageArray.getImageName().length == 1) {
            group.setImage(new XmlToImageMapper(imageArray.getImageName()[0], group).getImage());
        };
    }
}
