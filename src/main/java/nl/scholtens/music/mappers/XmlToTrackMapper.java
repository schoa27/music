package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.Record;
import nl.scholtens.music.domain.Track;
import nl.scholtens.music.xml.TrackXML;

public class XmlToTrackMapper {

    private Track track = new Track();

    public XmlToTrackMapper(TrackXML trackXML, Record record) {
        xmlMapper(trackXML,record);
    }

    public Track getTrack() {
        return track;
    }

    private void xmlMapper(TrackXML trackXML, Record record) {
        track.setSide(trackXML.getSide());
        track.setTitle(trackXML.getTitle());
        track.setTime(trackXML.getTime());
        track.setRecord(record);
    }
}
