package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.domain.Image;
import nl.scholtens.music.domain.Track;
import nl.scholtens.music.xml.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AlbumToXmlMapper {

    private AlbumXML albumXML = new AlbumXML();

    public AlbumToXmlMapper(Album album) {
        xmlMapper(album);
    }

    public AlbumXML getAlbumXML() {
        return albumXML;
    }

    private void xmlMapper(Album album) {
        albumXML.setCollection(album.isCollection());
        albumXML.setLabel(album.getLabel());
        albumXML.setTitle(album.getTitle());
        albumXML.setReleaseYear(album.getReleaseYear());
        albumXML.setMediaType(album.getMediaType());
        albumXML.setStyle(album.getStyle());
        albumXML.setRecordCount(album.getRecordCount());

        if (album.getArtist() != null) {
            mapArtistToXml(album);
        }

        if (album.getGroup() != null) {
            mapGroupToXml(album);
        }

        mapAlbumImages(album);
        mapRecordsAndTracksToXml(album);
    }

    private void mapArtistToXml(Album album) {
        albumXML.setArtist(new ArtistXML(album.getArtist().getFirstname(),
                album.getArtist().getLastname(),
                new ImageXML(new String[]{album.getArtist().getImage().getImageName()})));
    }

    private void mapGroupToXml(Album album) {
        albumXML.setGroup(new GroupXML(album.getGroup().getName(),
                new ImageXML(new String[]{album.getGroup().getImage().getImageName()})));
        
    }

    private void mapAlbumImages(Album album) {
        List<String> albumImages = album.getImages().stream()
                .map(Image::getImageName)
                .sorted((o1, o2) -> o1.toUpperCase().compareTo(o2.toUpperCase()))
                .collect(Collectors.toList());

        String[] imageNames = new String[albumImages.size()];

        for (int i = 0; i < albumImages.size(); i++) {
            imageNames[i] = albumImages.get(i);
        }

        albumXML.setImages(new ImageXML(imageNames));
    }

    private void mapRecordsAndTracksToXml(Album album) {
        album.getRecords().forEach(record -> albumXML.getRecords().add(new RecordXML(
                record.getNumber() != null ? record.getNumber() : null,
                record.getName() != null ? record.getName() : null,
                record.getImage() != null ? new ImageXML(new String[]{record.getImage().getImageName()}) : null,
                createTrackList(record.getTracks())
        )));
    }

    private List<TrackXML> createTrackList(List<Track> tracks) {
        List<TrackXML> trackList = new ArrayList<>();
        tracks.stream().forEach(track -> trackList.add(new TrackXML(track.getTitle(),
                track.getTime(),
                track.getSide())));
        return trackList;
    }
}
