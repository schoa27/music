package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.domain.Record;
import nl.scholtens.music.xml.ImageXML;
import nl.scholtens.music.xml.RecordXML;

import java.util.stream.Collectors;

public class XmlToRecordMapper {

    private Record record = new Record();

    public XmlToRecordMapper(RecordXML recordXML, Album album) {
        xmlMaper(recordXML, album);
    }

    public Record getRecord() {
        return record;
    }

    private void xmlMaper(RecordXML recordXML, Album album) {
        record.setName(recordXML.getName());
        record.setNumber(recordXML.getNumber());
        record.setAlbum(album);

        ImageXML imageArray = recordXML.getImage();
        if (imageArray != null && imageArray.getImageName().length == 1) {
            record.setImage(new XmlToImageMapper(imageArray.getImageName()[0], record).getImage());
        };

        if (!recordXML.getTracks().isEmpty()) {
            record.setTracks(recordXML.getTracks().stream()
                    .map(track -> new XmlToTrackMapper(track, record).getTrack())
                    .collect(Collectors.toList()));
        }
    }
}
