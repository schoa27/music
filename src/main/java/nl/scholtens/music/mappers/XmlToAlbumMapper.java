package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.xml.AlbumXML;

import java.util.Arrays;
import java.util.stream.Collectors;

public class XmlToAlbumMapper {

    private Album album = new Album();

    public XmlToAlbumMapper(AlbumXML albumXML) {
           xmlMapper(albumXML);
    }

    public Album getAlbum() {
        return album;
    }

    private void xmlMapper(AlbumXML albumXML) {
        if (albumXML.getGroup() != null) {
            album.setGroup(new XmlToGroupMapper(albumXML.getGroup())
                    .getGroup());
        }

        if (albumXML.getArtist() !=null) {
            album.setArtist(new XmlToArtistMapper(albumXML.getArtist())
                    .getArtist());
        }

        if (albumXML.getImages().getImageName() != null) {
            album.setImages(Arrays.stream(albumXML.getImages().getImageName())
                    .map(s -> new XmlToImageMapper(s, album).getImage())
                    .collect(Collectors.toList()));
        }

        if (!albumXML.getRecords().isEmpty()) {
            album.setRecords(albumXML.getRecords().stream()
                    .map(record -> new XmlToRecordMapper(record, album).getRecord())
                    .collect(Collectors.toList()));
        }

        album.setLabel(albumXML.getLabel());
        album.setCollection(albumXML.isCollection());
        album.setMediaType(albumXML.getMediaType());
        album.setStyle(albumXML.getStyle());
        album.setTitle(albumXML.getTitle());
        album.setReleaseYear(albumXML.getReleaseYear());
        album.setRecordCount(albumXML.getRecordCount());
    }
}
