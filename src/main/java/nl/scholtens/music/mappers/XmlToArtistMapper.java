package nl.scholtens.music.mappers;

import nl.scholtens.music.domain.Artist;
import nl.scholtens.music.xml.ArtistXML;
import nl.scholtens.music.xml.ImageXML;

public class XmlToArtistMapper {

    private Artist artist = new Artist();

    public XmlToArtistMapper(ArtistXML artistXML) {
        xmlMapper(artistXML);
    }

    public Artist getArtist() {
        return artist;
    }

    private void xmlMapper(ArtistXML artistXML) {
        artist.setFirstname(artistXML.getFirstname());
        artist.setLastname(artistXML.getLastname());

        ImageXML imageArray = artistXML.getImage();
        if (imageArray != null && imageArray.getImageName().length == 1) {
            artist.setImage(new XmlToImageMapper(imageArray.getImageName()[0], artist).getImage());
        };
    }
}
