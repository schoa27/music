package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "GP_GROUPS")
public class Group {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "GP_ID")
    private int id;

    @Column(name = "GP_NAME", length = 50)
    private String name;

//    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.PERSIST})
//    @JoinColumn(name = "GP_IMAGE",
//            foreignKey = @ForeignKey(name = "GP_IMAGE__IM_ID"))
    @OneToOne(mappedBy = "group",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE},
            orphanRemoval = true)
    private Image image;

    @OneToMany(fetch = FetchType.LAZY,
               cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE },
               mappedBy = "group",
               orphanRemoval = true)
    private List<Album> albums;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,CascadeType.REMOVE },
            mappedBy = "group",
            orphanRemoval = true)
    private List<Track> tracks;

    @Override
    public String toString() {
        return "Group{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image='" + image + '\'' +
                '}';
    }
}
