package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class MusicSession {

    private String buildVersion;

    private Map<String, String> sortItem = new HashMap<>();

    private String searchItem;

    private String searchFor;


    public MusicSession(String buildVersion) {
        this.buildVersion = buildVersion;

        sortItem.put("title","");
        sortItem.put("artistGroup","");
        sortItem.put("releaseYear","");
        sortItem.put("firstname","");
        sortItem.put("lastname", "");
        sortItem.put("name","");
    }
}
