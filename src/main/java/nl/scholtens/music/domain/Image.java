package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "IM_IMAGE")
public class Image {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IM_ID")
    private int id;

    @Column(name = "IM_NAME")
    private String imageName;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IM_AT_ID",
            referencedColumnName = "AT_ID",
            foreignKey = @ForeignKey(name = "IM_AT_ID__AT_ID"))
    private Artist artist;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IM_GP_ID",
            referencedColumnName = "GP_ID",
            foreignKey = @ForeignKey(name = "IM_GP_ID__GP_ID"))
    private Group group;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IM_RD_ID",
            referencedColumnName = "RD_ID",
            foreignKey = @ForeignKey(name = "IM_RD_ID__RD_ID"))
    private Record record;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "IM_AM_ID",
            referencedColumnName = "AM_ID",
            foreignKey = @ForeignKey(name = "IM_AM_ID__AM_ID"))
    private Album album;

    public Image() {
    }

    public Image(String imageName, Artist artist) {
        this.imageName = imageName;
        this.artist = artist;
    }

    public Image(String imageName, Group group) {
        this.imageName = imageName;
        this.group = group;
    }

    public Image(String imageName, Album album) {
        this.imageName = imageName;
        this.album = album;
    }

    public Image(String imageName, Record record) {
        this.imageName = imageName;
        this.record = record;
    }

}
