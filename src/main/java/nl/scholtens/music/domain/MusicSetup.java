package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MusicSetup {

    private String path[];
}
