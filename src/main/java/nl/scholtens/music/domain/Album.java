package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Entity
@Table(name = "AM_ALBUMS")
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AM_ID")
    private int id;

    @Column(name = "AM_COLLECTION", columnDefinition = "Boolean")
    private boolean collection;

    @Column(name = "AM_TITLE", length = 50)
    private String title;

    @Column(name = "AM_RELEASED")
    private String releaseYear;

    @Column(name = "AM_MEDIA", length = 20)
    @Enumerated(EnumType.STRING)
    private MediaType mediaType;

    @Column(name = "AM_STYLE", length = 20)
    @Enumerated(EnumType.STRING)
    private Style style;

    @Column(name = "AM_LABEL", length = 50)
    private String label;

    @OneToMany(mappedBy = "album", cascade = CascadeType.ALL)
    private List<Image> images = new ArrayList<>();

    @Column(name = "AM_RECORDS")
    private Integer recordCount;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "AM_AT_ID",
            foreignKey = @ForeignKey(name = "AM_AT_ID__AT_ID"))
    private Artist artist;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "AM_GP_ID",
            foreignKey = @ForeignKey(name = "AM_GP_ID__GP_ID"))
    private Group group;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.ALL},
            mappedBy = "album")
    private List<Record> records;

}
