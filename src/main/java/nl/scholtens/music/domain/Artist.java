package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "AT_ARTISTS")
public class Artist {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "AT_ID")
    private int id;

    @Column(name = "AT_FIRSTNAME", length = 50)
    private String firstname;

    @Column(name = "AT_LASTNAME", length = 50)
    private String lastname;

    @OneToOne(mappedBy = "artist",
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE},
            orphanRemoval = true)
    private Image image;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE},
            mappedBy = "artist",
            orphanRemoval = true)
    private List<Album> albums;

    @OneToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH, CascadeType.REMOVE},
            mappedBy = "artist",
            orphanRemoval = true)
    private List<Track> tracks;

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                '}';
    }
}
