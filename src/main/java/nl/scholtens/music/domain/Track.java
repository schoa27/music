package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "TR_TRACK")
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TR_ID")
    private int id;

    @Column(name = "TR_TITLE", length = 50)
    private String title;

    @Column(name = "TR_DURATION", length = 5)
    private String time;

    @Column(name = "TR_SIDE", length = 4)
    private String side;

    @OneToOne(fetch = FetchType.EAGER,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "TR_RD_ID",
            foreignKey = @ForeignKey(name = "TR_RD_ID__RD_ID"))
    private Record record;

    @OneToOne
    @JoinColumn(name = "TR_GP_ID",
            foreignKey = @ForeignKey(name = "TR_GP_ID__GP_ID"))
    private Group group;

    @OneToOne
    @JoinColumn(name = "TR_AT_ID",
            foreignKey = @ForeignKey(name = "TR_AT_ID__AT_ID"))
    private Artist artist;
}
