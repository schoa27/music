package nl.scholtens.music.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "RD_RECORDS")
public class Record {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "RD_ID")
    private Integer id;

    @OneToOne(orphanRemoval = true, cascade = CascadeType.ALL)
    @JoinColumn(name = "RD_AM_ID",
            foreignKey = @ForeignKey(name = "RD_AM_ID__AM_ID"))
    private Album album;

    @Column(name = "RD_NUMBER")
    private Integer number;

    @Column(name = "RD_NAME", length = 50)
    private String name;

    @OneToOne(mappedBy = "record",
            cascade = CascadeType.ALL)
    private Image image;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            mappedBy = "record")
    private List<Track> tracks;
}
