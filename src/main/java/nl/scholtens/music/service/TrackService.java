package nl.scholtens.music.service;

import nl.scholtens.music.domain.Track;

public interface TrackService {

    Track saveTrack(Track track);

    Track getTrackById(Integer trackId);

    void deleteTrackById(Integer trackId);

    Integer countTrackByRecordId(Integer recordId);
}
