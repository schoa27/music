package nl.scholtens.music.service;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;
import nl.scholtens.music.repository.AlbumRepository;
import nl.scholtens.music.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumRepository albumRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public List<Album> getAllAlbums() {
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumsSorted(String sortItem, String sortType) {
        switch (sortItem) {
            case "title":
                return (sortType.equals("asc") ? albumRepository.findAlbumsByOrderByTitleAsc()
                        : albumRepository.findAlbumsByOrderByTitleDesc());
            case "releaseYear":
                return (sortType.equals("asc") ? albumRepository.findAlbumsByOrderByReleaseYearAsc()
                        : albumRepository.findAlbumsByOrderByReleaseYearDesc());
            case "artistGroup":
                List<Album> albums = (List<Album>) albumRepository.findAll();
                List<Album> list = new ArrayList<>();

                if (sortType.equals("asc")) {
                    albums.stream()
                            .filter(album -> album.isCollection())
                            .forEach(album -> list.add(album));
                    albums.stream().filter(album -> album.getArtist() != null)
                            .sorted(getAlbumArtistComparator())
                            .forEach(album -> list.add(album));
                    albums.stream()
                            .filter(album -> album.getGroup() != null)
                            .sorted(getAlbumGroupComparator())
                            .forEach(album -> list.add(album));
                } else {
                    albums.stream().filter(album -> album.getArtist() != null)
                            .sorted(getAlbumArtistComparator().reversed())
                            .forEach(album -> list.add(album));

                    albums.stream()
                            .filter(album -> album.getGroup() != null)
                            .sorted(getAlbumGroupComparator().reversed())
                            .forEach(album -> list.add(album));
                    albums.stream()
                            .filter(album -> album.isCollection())
                            .forEach(album -> list.add(album));
                }
                return list;
        }

        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumSearchByTitleSorted(String sortItem, String sortType, String searchFor) {
        switch (sortItem) {
            case "title":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByTitleContainingIgnoreCaseOrderByTitleAsc(searchFor)
                        : albumRepository.findAlbumsByTitleContainingIgnoreCaseOrderByTitleDesc(searchFor));
            case "releaseYear":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByTitleContainingIgnoreCaseOrderByReleaseYearAsc(searchFor)
                        : albumRepository.findAlbumsByTitleContainingIgnoreCaseOrderByReleaseYearDesc(searchFor));
            case "artistGroup":
                if (sortType.equals("asc")) {
                    List<Album> listAt =
                            albumRepository.findAlbumsByTitleContainingIgnoreCaseAndArtistNotNullOrderByArtistAsc(searchFor);
                    List<Album> listGp =
                            albumRepository.findAlbumsByTitleContainingIgnoreCaseAndGroupNotNullOrderByGroupAsc(searchFor);
                    listAt.addAll(listGp);
                    return listAt;
                } else {
                    List<Album> listAt =
                            albumRepository.findAlbumsByTitleContainingIgnoreCaseAndArtistNotNullOrderByArtistDesc(searchFor);
                    List<Album> listGp =
                            albumRepository.findAlbumsByTitleContainingIgnoreCaseAndGroupNotNullOrderByGroupDesc(searchFor);
                    listAt.addAll(listGp);
                    return listAt;
                }


        }
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumSearchByArtistSorted(String sortItem, String sortType, String searchFor) {
        List<Album> list = new ArrayList<>();
        switch (sortItem) {
            case "title":
                if (sortType.equals("asc")) {
                    List<Album> byArtistFirstnameTitle =
                            albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByTitleAsc(searchFor);
                    List<Album> byArtistLastnameTitle =
                            albumRepository.findAlbumsByArtistLastnameContainingIgnoreCaseOrderByTitleAsc(searchFor);
                    return createAlbumListSortedByArtist(byArtistFirstnameTitle, byArtistLastnameTitle, list);
                }

                List<Album> byArtistFirstnameTitle =
                        albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByTitleDesc(searchFor);
                List<Album> byArtistLastnameTitle =
                        albumRepository.findAlbumsByArtistLastnameContainingIgnoreCaseOrderByTitleDesc(searchFor);
                return createAlbumListSortedByArtist(byArtistFirstnameTitle, byArtistLastnameTitle, list);
            case "artistGroup":
                if (sortType.equals("asc")) {
                    List<Album> byArtistFirstnameArtist =
                            albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByArtistAsc(searchFor);
                    List<Album> byArtistLastnameArtist =
                            albumRepository.findAlbumsByArtistLastnameContainingIgnoreCaseOrderByArtistAsc(searchFor);
                    return createAlbumListSortedByArtist(byArtistFirstnameArtist, byArtistLastnameArtist, list);
                }

                List<Album> byArtistFirstnameArtist =
                        albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByArtistDesc(searchFor);
                List<Album> byArtistLastnameArtist =
                        albumRepository.findAlbumsByArtistLastnameContainingIgnoreCaseOrderByArtistDesc(searchFor);
                return createAlbumListSortedByArtist(byArtistFirstnameArtist, byArtistLastnameArtist, list);
            case "releaseYear":
                if (sortType.equals("asc")) {
                    List<Album> byArtistFirstnameYear =
                            albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByReleaseYearAsc(searchFor);
                    List<Album> byArtistLastnameYear =
                            albumRepository.findAlbumsByArtistLastnameContainingIgnoreCaseOrderByReleaseYearAsc(searchFor);
                    return createAlbumListSortedByArtist(byArtistFirstnameYear, byArtistLastnameYear, list);
                }

                List<Album> byArtistFirstnameYear =
                        albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCaseOrderByReleaseYearDesc(searchFor);
                List<Album> byArtistLastnameYear =
                        albumRepository.findAlbumsByArtistLastnameContainingIgnoreCaseOrderByReleaseYearDesc(searchFor);
                return createAlbumListSortedByArtist(byArtistFirstnameYear, byArtistLastnameYear, list);
        }
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumSearchByGrouptSorted(String sortItem, String sortType, String searchFor) {
        switch (sortItem) {
            case "title":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByGroupNameContainingIgnoreCaseOrderByTitleAsc(searchFor)
                        : albumRepository.findAlbumsByGroupNameContainingIgnoreCaseOrderByTitleDesc(searchFor));
            case "releaseYear":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByGroupNameContainingIgnoreCaseOrderByReleaseYearAsc(searchFor)
                        : albumRepository.findAlbumsByGroupNameContainingIgnoreCaseOrderByReleaseYearDesc(searchFor));
            case "artistGroup":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByGroupNameContainingIgnoreCaseOrderByGroupAsc(searchFor)
                        : albumRepository.findAlbumsByGroupNameContainingIgnoreCaseOrderByGroupDesc(searchFor));
        }
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumSearchByReleaseYearSorted(String sortItem, String sortType, String searchFor) {
        switch (sortItem) {
            case "title":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseOrderByTitleAsc(searchFor)
                        : albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseOrderByTitleDesc(searchFor));
            case "releaseYear":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseOrderByReleaseYearAsc(searchFor)
                        : albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseOrderByReleaseYearDesc(searchFor));
            case "artistGroup":
                if (sortType.equals("asc")) {
                    List<Album> listAt =
                            albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseAndArtistNotNullOrderByArtistAsc(searchFor);
                    List<Album> listGp =
                            albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseAndGroupNotNullOrderByGroupAsc(searchFor);
                    listAt.addAll(listGp);
                    return listAt;
                } else {
                    List<Album> listAt =
                            albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseAndArtistNotNullOrderByArtistDesc(searchFor);
                    List<Album> listGp =
                            albumRepository.findAlbumsByReleaseYearContainingIgnoreCaseAndGroupNotNullOrderByGroupDesc(searchFor);
                    listAt.addAll(listGp);
                    return listAt;
                }
        }
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumSearchByMediaTypetSorted(String sortItem, String sortType, String searchFor) {
        switch (sortItem) {
            case "title":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByMediaTypeOrderByTitleAsc(MediaType.valueOf(searchFor))
                        : albumRepository.findAlbumsByMediaTypeOrderByTitleDesc(MediaType.valueOf(searchFor)));
            case "releaseYear":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByMediaTypeOrderByReleaseYearAsc(MediaType.valueOf(searchFor))
                        : albumRepository.findAlbumsByMediaTypeOrderByReleaseYearDesc(MediaType.valueOf(searchFor)));
            case "artistGroup":
                if (sortType.equals("asc")) {
                    List<Album> listAt = albumRepository.findAlbumsByMediaTypeAndArtistNotNullOrderByArtistAsc(MediaType.valueOf(searchFor));
                    List<Album> listGp = albumRepository.findAlbumsByMediaTypeAndGroupNotNullOrderByGroupAsc(MediaType.valueOf(searchFor));
                    listAt.addAll(listGp);
                    return listAt;
                } else {
                    List<Album> listAt = albumRepository.findAlbumsByMediaTypeAndArtistNotNullOrderByArtistDesc(MediaType.valueOf(searchFor));
                    List<Album> listGp = albumRepository.findAlbumsByMediaTypeAndGroupNotNullOrderByGroupDesc(MediaType.valueOf(searchFor));
                    listAt.addAll(listGp);
                    return listAt;
                }
        }
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> getAllAlbumSearchByStyleSorted(String sortItem, String sortType, String searchFor) {
        switch (sortItem) {
            case "title":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByStyleOrderByTitleAsc(Style.valueOf(searchFor))
                        : albumRepository.findAlbumsByStyleOrderByTitleDesc(Style.valueOf(searchFor)));
            case "releaseYear":
                return (sortType.equals("asc")
                        ? albumRepository.findAlbumsByStyleOrderByReleaseYearAsc(Style.valueOf(searchFor))
                        : albumRepository.findAlbumsByStyleOrderByReleaseYearDesc(Style.valueOf(searchFor)));
            case "artistGroup":
                if (sortType.equals("asc")) {
                    List<Album> listAt = albumRepository.findAlbumsByStyleAndArtistNotNullOrderByArtistAsc(Style.valueOf(searchFor));
                    List<Album> listGp = albumRepository.findAlbumsByStyleAndGroupNotNullOrderByGroupAsc(Style.valueOf(searchFor));
                    listAt.addAll(listGp);
                    return listAt;
                } else {
                    List<Album> listAt = albumRepository.findAlbumsByStyleAndArtistNotNullOrderByArtistDesc(Style.valueOf(searchFor));
                    List<Album> listGp = albumRepository.findAlbumsByStyleAndGroupNotNullOrderByGroupDesc(Style.valueOf(searchFor));
                    listAt.addAll(listGp);
                    return listAt;
                }
        }
        return (List<Album>) albumRepository.findAll();
    }

    @Override
    public List<Album> findAlbumByTitle(String searchString) {
        return albumRepository.findByTitleContainingIgnoreCase(searchString);
    }

    @Override
    public List<Album> findAlbumByArtist(String searchString) {
        List<Album> albums = albumRepository.findAlbumsByArtistFirstnameContainingIgnoreCase(searchString);
        albums.addAll(albumRepository.findAlbumsByArtistLastnameContainingIgnoreCase(searchString));
        return albums;
    }

    @Override
    public List<Album> findAlbumByGroup(String searchString) {
        return albumRepository.findAlbumsByGroupNameContainingIgnoreCase(searchString);
    }

    @Override
    public List<Album> findAlbumByReleaseYear(String year) {
        return albumRepository.findAlbumsByReleaseYear(year);
    }

    @Override
    public List<Album> findAlbumsByMedia(MediaType type) {
        return albumRepository.findAlbumsByMediaType(type);
    }

    @Override
    public List<Album> findAlbumsByStyle(Style listStyle) {
        return albumRepository.findAlbumsByStyle(listStyle);
    }

    @Override
    public void saveAlbum(Album album) {
        albumRepository.save(album);
    }

    @Override
    public Album getAlbumById(Integer id) {
        return albumRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteAlbum(Album album) {
        albumRepository.delete(album);
    }

    @Override
    public void deleteImages(int albumId) {
        imageRepository.delAlbumImages(albumId);
    }

    @Override
    public List<Integer> getAllAlbumIds() {
        return albumRepository.getAlAlbumIds();
    }

    @Override
    public List<Album> getAllAlbumsOfArtist(String name) {
        List<Album> albums = albumRepository.getAllAlbumFromArtistByFirstnameAndLastname(name);
        return albums;
    }

    @Override
    public void updateAlbum(Album album) {
        Album albumById = albumRepository.findAlbumById(album.getId());
        albumById.setStyle(album.getStyle());
        albumById.setReleaseYear(album.getReleaseYear());
        albumById.setArtist(album.getArtist());
        albumById.setLabel(album.getLabel());
        albumById.setGroup(album.getGroup());
        albumById.setMediaType(album.getMediaType());
        albumById.setCollection(album.isCollection());
        albumById.setRecordCount(album.getRecordCount());
        albumById.setRecords(album.getRecords());
        albumById.setImages(album.getImages());

        albumRepository.save(albumById);
    }

    private Comparator<Album> getAlbumGroupComparator() {
        return (s1, s2) -> s1.getGroup().getName().compareTo(s2.getGroup().getName());
    }

    private Comparator<Album> getAlbumArtistComparator() {
        return (s1, s2) -> s1.getArtist().getLastname().compareTo(s2.getArtist().getLastname());
    }

    private List<Album> createAlbumListSortedByArtist(List<Album> albumByArtistFirstname, List<Album> albumsByArtistLastname, List<Album> list) {
        list.addAll(albumByArtistFirstname);
        albumsByArtistLastname.stream()
                .forEach(albumf -> list.add(albumByArtistFirstname.stream()
                        .filter(albuml -> albuml.getId() != albumf.getId())
                        .findFirst()
                        .get()));
        return list;
    }
}
