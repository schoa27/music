package nl.scholtens.music.service;

import nl.scholtens.music.domain.Record;

import java.util.List;

public interface RecordService {

    List<Record> getRecordByAlbumId(Integer id);

    Record getRecordByAlbumIdAndRecordNumber(Integer albumId, Integer number);

    Integer countRecords(int albumId);

    Record getRecordById(int recordId);

    Record findRecordByAlbumIdAndRecordNumber(Integer albumId, Integer recordNumber);

    void deleteRecordById(Integer recordId);
}
