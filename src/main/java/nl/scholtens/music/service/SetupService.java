package nl.scholtens.music.service;

public interface SetupService {

    boolean writeSetupFile(String[] paths);

    String[] readSetupFile();

    byte[] getImageFile(String image);

    void deleteImagesFiles(String[] files);
}
