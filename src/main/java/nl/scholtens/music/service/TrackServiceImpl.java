package nl.scholtens.music.service;

import nl.scholtens.music.domain.Track;
import nl.scholtens.music.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    @Autowired
    private TrackRepository trackRepository;

    @Override
    public Track saveTrack(Track track) {
        Track save = trackRepository.save(track);
        return save;
    }

    @Override
    public Track getTrackById(Integer trackId) {
        return (trackId != null) ? trackRepository.findById(trackId).orElse(null) : null;
    }

    @Override
    public void deleteTrackById(Integer trackId) {
        trackRepository.deleteByTrackId(trackId);
    }

    @Override
    public Integer countTrackByRecordId(Integer recordId) {
        return trackRepository.countTrackByRecordId(recordId);
    }
}
