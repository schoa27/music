package nl.scholtens.music.service;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AlbumService {

    List<Album> getAllAlbums();

    List<Album> getAllAlbumsSorted(String sortItem, String sortType);

    List<Album> findAlbumByTitle(String title);

    List<Album> findAlbumByArtist(String searchString);

    List<Album> findAlbumByGroup(String searchString);

    List<Album> findAlbumByReleaseYear(String year);

    List<Album> findAlbumsByMedia(MediaType type);

    List<Album> findAlbumsByStyle(Style listStyle);

    List<Album> getAllAlbumSearchByTitleSorted(String sortItem, String sortType, String searchFor);

    List<Album> getAllAlbumSearchByArtistSorted(String sortItem, String sortType, String searchFor);

    List<Album> getAllAlbumSearchByGrouptSorted(String sortItem, String sortType, String searchFor);

    List<Album> getAllAlbumSearchByReleaseYearSorted(String sortItem, String sortType, String searchFor);

    List<Album> getAllAlbumSearchByMediaTypetSorted(String sortItem, String sortType, String searchFor);

    List<Album> getAllAlbumSearchByStyleSorted(String sortItem, String sortType, String searchFor);

    void saveAlbum(Album album);

    Album getAlbumById(Integer id);

    void deleteAlbum(Album album);

    void deleteImages(int albumId);

    List<Integer> getAllAlbumIds();

    List<Album> getAllAlbumsOfArtist(String name);

    void updateAlbum(Album albumByTitle);
}
