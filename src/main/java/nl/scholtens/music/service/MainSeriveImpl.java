package nl.scholtens.music.service;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.repository.AlbumRepository;
import nl.scholtens.music.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class MainSeriveImpl implements MainService {

    @Autowired
    ImageRepository imageRepository;


    @Autowired
    AlbumRepository albumRepository;

    @Override
    public List<Integer> allIdOfAllAlbums() {
        return albumRepository.getAlAlbumIds();
    }

    @Override
    public Album getAlBumById(int id) {
        return albumRepository.findAlbumById(id);
      }

}
