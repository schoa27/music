package nl.scholtens.music.service;

import nl.scholtens.music.domain.Album;

import java.util.List;

public interface MainService {

    List<Integer> allIdOfAllAlbums();

    Album getAlBumById(int id);
}
