package nl.scholtens.music.service;

import nl.scholtens.music.domain.Group;

import java.util.List;


public interface GroupService {

    List<Group> getAllGroups();

    List<Group> findGroupByName(String name);

    List<Group> getAllGroupsSorted(String sortItem, String sortType);

    List<Group> findGroupByNameSorted(String sortItem, String sortType, String searchFor);

    Group getGroupById(Integer id);

    void saveGroup(Group group);

    void deleteGroup(Group group);

    void delImage(int groupId);
}
