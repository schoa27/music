package nl.scholtens.music.service;

import nl.scholtens.music.domain.Artist;
import nl.scholtens.music.repository.ArtistRepository;
import nl.scholtens.music.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ArtistServiceImpl implements ArtistService {

    @Autowired
    private ArtistRepository artistRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public List<Artist> findArtistByName(String name) {
        List<Artist> resultList = new ArrayList<>();
        resultList.addAll(artistRepository.findArtistsByFirstnameContainingIgnoreCase(name));
        resultList.addAll(artistRepository.findArtistsByLastnameContainingIgnoreCase(name));
        return resultList.stream().distinct().collect(Collectors.toList());
    }

    @Override
    public List<Artist> getAllArtists() {
        return (List<Artist>) artistRepository.findAll();
    }

    @Override
    public List<Artist> getAllArtistsSorted(String sortItem, String sortType) {
        switch (sortItem) {
            case "firstname":
                return (sortType.equals("asc") ? artistRepository.findArtistsByOrderByFirstnameAsc()
                                                :artistRepository.findArtistsByOrderByFirstnameDesc());
            case "lastname":
                return (sortType.equals("asc") ? artistRepository.findArtistsByOrderByLastnameAsc()
                                                :artistRepository.findArtistsByOrderByLastnameDesc());
        }
        return (List<Artist>) artistRepository.findAll();
    }

    @Override
    public Artist getArtistById(Integer id) {
        return artistRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteArtist(Artist artist) {
       artistRepository.delete(artist);
    }

    @Override
    public void delImage(int artistId) {
        imageRepository.delArtistImage(artistId);
    }

    @Override
    public void saveArtist(Artist artist) {
        artistRepository.save(artist);
    }
}
