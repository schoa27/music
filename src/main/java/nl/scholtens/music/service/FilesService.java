package nl.scholtens.music.service;

import org.springframework.web.multipart.MultipartFile;

public interface FilesService {
    void uploadFile(MultipartFile file, String filePath);

    void createMaps(String[] fileMaps);
}
