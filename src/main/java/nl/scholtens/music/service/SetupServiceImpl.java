package nl.scholtens.music.service;

import org.springframework.stereotype.Service;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Arrays;


@Service
public class SetupServiceImpl implements SetupService {

    private final String FILE_NAME = "music.ini";

    @Override
    public boolean writeSetupFile(String[] lines) {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(FILE_NAME))) {
            if (lines.length > 0) {
               for (int i = 1; i <= lines.length; i++) {
                    writer.write(lines[i - 1]);
                    writer.newLine();
                }

            }
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String[] readSetupFile() {
        try (BufferedReader read = Files.newBufferedReader(Paths.get(FILE_NAME))) {
            String[] lines = read.lines().toArray(String[]::new);
            return lines;
        } catch (NoSuchFileException e) {
            makeIniFile();
            return new String[0];
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public byte[] getImageFile(String image) {
        String[] paths = readSetupFile();
        File file = new File(paths[0] + image);
        try {
            return Files.readAllBytes(file.toPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void deleteImagesFiles(String[] files) {
        String[] paths = readSetupFile();
        for (int i = 0; i < files.length; i++) {
            try {
                Files.delete(Paths.get(paths[0] + files[i]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void makeIniFile() {
        try {
            new FileWriter(FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
