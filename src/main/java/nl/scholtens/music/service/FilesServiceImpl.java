package nl.scholtens.music.service;

import nl.scholtens.music.exceptions.FileStorageException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;

@Service
public class FilesServiceImpl implements FilesService {

    @Override
    public void uploadFile(MultipartFile file, String path) {
        try {
            if (!file.getOriginalFilename().isEmpty()) {
                Path fileLocation = Paths.get(path
                        + File.separator + StringUtils.cleanPath(file.getOriginalFilename()));
                Files.copy(file.getInputStream(), fileLocation, StandardCopyOption.REPLACE_EXISTING);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new FileStorageException("File Not Found");
        }
    }

    @Override
    public void createMaps(String[] fileMaps) {

        Arrays.asList(fileMaps).stream().forEach(fileMap -> {
            try {
                if (!Files.isDirectory(Paths.get(fileMap))) {
                    Files.createDirectory(Paths.get(fileMap));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
