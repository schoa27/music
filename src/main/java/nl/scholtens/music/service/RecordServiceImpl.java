package nl.scholtens.music.service;

import nl.scholtens.music.domain.Record;
import nl.scholtens.music.repository.RecordRepository;
import nl.scholtens.music.repository.TrackRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordServiceImpl implements RecordService {

    @Autowired
    RecordRepository recordRepository;

    @Autowired
    TrackRepository trackRepository;

    @Override
    public List<Record> getRecordByAlbumId(Integer id) {
        List<Record> records = recordRepository.findRecordByAlbum_IdOrderByNumber(id);
        records.stream().forEach(record -> record.setTracks(trackRepository.getAllTracksByRecordId(record.getId())));
        return records;
    }

    @Override
    public Record getRecordByAlbumIdAndRecordNumber(Integer albumId, Integer number) {
        List<Record> recordList = recordRepository.findRecordByAlbum_IdOrderByNumber(albumId);
        Record record = recordList.stream()
                .filter(r -> r.getNumber() == number)
                .findFirst()
                .orElse(null);
        return record;
    }

    @Override
    public Integer countRecords(int albumId) {
        return recordRepository.recordCountByAlbumId(albumId);
    }

    @Override
    public Record getRecordById(int trackId) {
        Integer recordId = trackRepository.findRecordIdByTrackId(trackId);
        return recordRepository.findRecordById(recordId);
    }

    @Override
    public Record findRecordByAlbumIdAndRecordNumber(Integer albumId, Integer recordNumber) {
        return recordRepository.findRecordByAlbum_IdAndNumber(albumId, recordNumber);
    }

    @Override
    public void deleteRecordById(Integer recordId) {
        recordRepository.deleteRecordById(recordId);
    }
}
