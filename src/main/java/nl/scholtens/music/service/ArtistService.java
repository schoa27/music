package nl.scholtens.music.service;

import nl.scholtens.music.domain.Artist;

import java.util.List;

public interface ArtistService {

    List<Artist> findArtistByName(String name);

    List<Artist> getAllArtists();

    List<Artist> getAllArtistsSorted(String sortItem, String sortType);

    Artist getArtistById(Integer id);

    void saveArtist(Artist artist);

    void deleteArtist(Artist artist);

    void delImage(int artistId);
}
