package nl.scholtens.music.service;

import nl.scholtens.music.domain.Group;
import nl.scholtens.music.repository.GroupRepository;
import nl.scholtens.music.repository.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class GroupServiceImpl implements GroupService {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private ImageRepository imageRepository;

    @Override
    public List<Group> getAllGroups() {
        return (List<Group>) groupRepository.findAll();
    }

    @Override
    public List<Group> findGroupByName(String name) {
        return groupRepository.findByNameContainingIgnoreCase(name);
    }

    @Override
    public List<Group> getAllGroupsSorted(String sortItem, String sortType) {
        return (sortType.equals("asc") ? groupRepository.findGroupByOrderByNameAsc()
                                       : groupRepository.findGroupByOrderByNameDesc());
    }

    @Override
    public List<Group> findGroupByNameSorted(String sortItem, String sortType, String searchFor) {
        return (sortType.equals("asc") ? groupRepository.findByNameContainingIgnoreCaseOrderByNameAsc(searchFor)
                                       : groupRepository.findByNameContainingIgnoreCaseOrderByNameDesc(searchFor));
    }

    @Override
    public Group getGroupById(Integer id) {
        return groupRepository.findById(id).orElse(null);
    }

    @Override
    public void saveGroup(Group group) {
        groupRepository.save(group);
    }

    @Override
    public void deleteGroup(Group group) {
        groupRepository.delete(group);
    }

    @Override
    public void delImage(int groupId) {
        imageRepository.delGroupImage(groupId);
    }
}
