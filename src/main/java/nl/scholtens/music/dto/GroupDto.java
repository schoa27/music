package nl.scholtens.music.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class GroupDto {

    private int id;

    private String name;

    private String image;

    private MultipartFile multipartFile;

    private int imageId;
}
