package nl.scholtens.music.dto;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@Setter
public class MusicSearchDto {

    @Size(min = 1, message = "Zoekveld moet gevuld zijn")
    private String searchFor;

    private String searchItem;

    private String choose;

    private String media;

    private String style;

    private List<MediaType> mediaTypes = new ArrayList<>(Arrays.asList(MediaType.values()));

    private List<Style> styles = new ArrayList<>(Arrays.asList(Style.values()));

    public MusicSearchDto() {
        super();
    }

    public MusicSearchDto(String searchItem) {
        this.searchItem = searchItem;
    }

}

