package nl.scholtens.music.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

@Getter
@Setter
public class ArtistDto {

    private int id;

    private String firstname;

    private String lastname;

    private String image;

    private MultipartFile multipartFile;

    private int imageId;

}
