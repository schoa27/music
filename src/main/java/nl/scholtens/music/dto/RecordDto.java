package nl.scholtens.music.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class RecordDto {

    private Integer number;

    private String side;

    private String title;

    private String durationMin;

    private String durationSec;

    private String duration;

    private String artist;

    private String group;

    private String image;

    private List<TrackDto> trackList = new ArrayList<>();

    public String getDuration() {
        return durationMin + ":" + durationSec;
    }

    public void setDuration(String duration) {
        String[] splitString = duration.split(":");
        this.durationMin = splitString[0];
        this.durationSec = splitString[1];
    }

}
