package nl.scholtens.music.dto;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.domain.Artist;
import nl.scholtens.music.domain.Group;

@Getter
@Setter
public class TrackDto {

    private int id;

    private String title;

    private String time;

    private String side;

    private Group group;

    private Artist artist;
}
