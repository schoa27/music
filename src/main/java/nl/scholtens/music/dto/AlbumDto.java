package nl.scholtens.music.dto;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AlbumDto {

    private int id;

    private String title;

    private String releaseYear;

    private String media;

    private MediaType mediaType;

    private String label;

    private String style;

    private Style styleType;

    private String image;

    private String images[];

    private MultipartFile[] multipartFiles;

    private boolean collection;

    private String artist;

    private String artistId;

    private String group;

    private String groupId;

    private Integer totalRecords;

    private List<Integer> imageIds = new ArrayList<>();
}
