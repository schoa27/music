package nl.scholtens.music.From;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.enums.Constant;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class BeheerForm {

    private boolean importBeheer;

    private boolean exportBeheer;

    private List<String> albums = new ArrayList<>();

    private List<String> groups = new ArrayList<>();

    private List<String> artists = new ArrayList<>();

    private List<String> mediaTypes = new ArrayList();

    private String album;

    private String artist;

    private String group;

    private String media;

    private String allMedia;

    private MultipartFile[] xmlMultipartFiles;

    private MultipartFile[] imgMultipartFiles;

    private int total;

    private boolean all;

    private boolean result;

    private String fieldset;

    private String filePath = Constant.XML_PATH;

    public BeheerForm() {
        super();
    }

    public BeheerForm(boolean importBeheer, boolean exportBeheer) {
        this.importBeheer = importBeheer;
        this.exportBeheer = exportBeheer;
    }

}
