package nl.scholtens.music.From;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.dto.RecordDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class RecordForm {

    private RecordDto record = new RecordDto();

    private boolean albumform = false;

    private Integer albumId;

    private Integer trackId;

    private boolean collection;

    private String mediaType;

    private boolean side;

    private Integer recordCount;

    private boolean recordImage;

    private Integer currentRecord;

    private List<Integer> recordnumbers = new ArrayList<>();

    private List<String> sides = new ArrayList<>();

    private List<RecordDto> records = new ArrayList<>();
}
