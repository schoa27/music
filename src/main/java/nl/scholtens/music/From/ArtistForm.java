package nl.scholtens.music.From;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.dto.ArtistDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class ArtistForm {

    private List<ArtistDto> artistsList;

    private ArtistDto artist;

    private boolean updateArtist;

    private boolean delImage;

    private int id;
}
