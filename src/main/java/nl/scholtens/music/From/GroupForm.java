package nl.scholtens.music.From;

import lombok.Getter;
import lombok.Setter;
import nl.scholtens.music.dto.GroupDto;

import java.util.List;

@Setter
@Getter
public class GroupForm {

    private List<GroupDto> groupsList;

    private GroupDto group;

    private boolean updateGroup;

    private boolean delImage;

    private int id;
}
