package nl.scholtens.music.From;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import nl.scholtens.music.domain.Artist;
import nl.scholtens.music.domain.Group;
import nl.scholtens.music.dto.AlbumDto;
import nl.scholtens.music.dto.RecordDto;
import nl.scholtens.music.enums.MediaType;
import nl.scholtens.music.enums.Style;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class AlbumForm {

    private List<AlbumDto> albumList = new ArrayList<>();

    private AlbumDto album;

    private List<Artist> artists;

    private List<Group> groups;

    private List<MediaType> mediaTypes;

    private List<Style> styles;

    private List<RecordDto> records = new ArrayList<>();

    private int id;

    private String recordsInBox;

    private boolean albumform = true;

    private boolean side;

    private boolean addAlbum;

    private boolean delImage;

    public AlbumForm() {
        super();
    }

    public AlbumForm(List<Artist> artists, List<Group> groups, List<MediaType> mediaTypes, List<Style> styles) {
        this.artists = artists;
        this.groups = groups;
        this.mediaTypes = mediaTypes;
        this.styles = styles;
    }
}
