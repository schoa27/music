package nl.scholtens.music.parsers;

import nl.scholtens.music.domain.Album;
import nl.scholtens.music.enums.Constant;
import nl.scholtens.music.mappers.AlbumToXmlMapper;
import nl.scholtens.music.xml.AlbumXML;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class XmlParser {

    public static void parsToXML(Album album) {
        AlbumToXmlMapper albumMapper = new AlbumToXmlMapper(album);
        AlbumXML albumXML = albumMapper.getAlbumXML();

        String filename = album.getTitle().replaceAll("\\s+", "")
                + album.getMediaType().getMediaType().replaceAll("\\s+", "")
                    .replaceAll("/","");
        File file = new File(Constant.XML_PATH + filename + ".xml");

        JAXBContext jaxbContext = null;
        try {
            jaxbContext = JAXBContext.newInstance(AlbumXML.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            FileWriter writer = new FileWriter(file);
            jaxbMarshaller.marshal(albumXML, writer);
        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static AlbumXML parsFromXML(String filename) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(AlbumXML.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File("src/main/resources/xsd/album.xsd"));
            unmarshaller.setSchema(schema);

            AlbumXML albumXML = (AlbumXML) unmarshaller.unmarshal(new File(Constant.XML_PATH + filename));
            return albumXML;

        } catch (JAXBException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return null;
    }
}
